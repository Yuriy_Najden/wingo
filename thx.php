<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="" />
    <meta property="og:description" content="Доставка грузов «под ключ»" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="/img/og.jpg" />

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#001066">

    <meta name="theme-color" content="#001066">

    <title>Wingo</title>

    <link rel="stylesheet" href="css/likely.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css"/>

</head>
<body>
<div class="wrapper thx-page">
    <header>
        <div class="top-line">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <div class="logo">
                            <img src="img/logo.svg" alt="">
                        </div>
                        <a class="mob-menu" id="mob-menu">
                            <div class="top"></div>
                            <div class="center"></div>
                            <div class="bottom"></div>
                        </a>
                        <div class="mob-contacts">
                            <img src="img/scroll-logo.svg" alt="" class="menu-logo">
                            <div class="center-conteiner">
                                <h3>контакты</h3>
                                <div class="phone-wrapper">
                                    <a href="tel:380672493154">+38(067) 249-31-54</a>
                                    <a href="tel:380445947333">+38(044) 594-73-33</a>
                                </div>
                                <div class="soc-wrapper">
                                    <a href="#">
                                        <svg width="11" height="22" viewBox="0 0 11 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0.322864 11.6827H2.51715V21.0775C2.51715 21.263 2.66165 21.4133 2.84001 21.4133H6.56045C6.73881 21.4133 6.88331 21.263 6.88331 21.0775V11.727H9.40582C9.56983 11.727 9.7078 11.599 9.72653 11.4295L10.1096 7.97083C10.1202 7.87568 10.0912 7.78038 10.03 7.70901C9.9687 7.63753 9.881 7.59663 9.78897 7.59663H6.88343V5.42858C6.88343 4.77499 7.22182 4.4436 7.88924 4.4436C7.98437 4.4436 9.78897 4.4436 9.78897 4.4436C9.96733 4.4436 10.1118 4.29324 10.1118 4.10778V0.933047C10.1118 0.747551 9.96733 0.597268 9.78897 0.597268H7.17087C7.15238 0.596333 7.11137 0.594788 7.05096 0.594788C6.59665 0.594788 5.01768 0.687535 3.77037 1.8809C2.38836 3.20336 2.58049 4.78674 2.62639 5.06129V7.59659H0.322864C0.144503 7.59659 0 7.74687 0 7.93237V11.3468C0 11.5323 0.144503 11.6827 0.322864 11.6827Z" fill="white"/>
                                        </svg>
                                    </a>
                                    <a href="#">
                                        <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.444 0H7.54852C3.93334 0 0.992188 2.94116 0.992188 6.55633V15.4518C0.992188 19.0669 3.93334 22.0081 7.54852 22.0081H16.444C20.0591 22.0081 23.0003 19.0669 23.0003 15.4518V6.55633C23.0003 2.94116 20.0591 0 16.444 0ZM20.7863 15.4518C20.7863 17.8499 18.8421 19.7941 16.444 19.7941H7.54852C5.15034 19.7941 3.2062 17.8499 3.2062 15.4518V6.55633C3.2062 4.15812 5.15034 2.21401 7.54852 2.21401H16.444C18.8421 2.21401 20.7863 4.15812 20.7863 6.55633V15.4518Z" fill="white"/>
                                            <path d="M11.9957 5.31232C8.85714 5.31232 6.30371 7.8661 6.30371 11.0051C6.30371 14.144 8.85714 16.6979 11.9957 16.6979C15.1343 16.6979 17.6877 14.1441 17.6877 11.0051C17.6877 7.86606 15.1343 5.31232 11.9957 5.31232ZM11.9957 14.4836C10.0749 14.4836 8.5177 12.9262 8.5177 11.0051C8.5177 9.08397 10.0749 7.52661 11.9957 7.52661C13.9166 7.52661 15.4738 9.08397 15.4738 11.0051C15.4738 12.9262 13.9166 14.4836 11.9957 14.4836Z" fill="white"/>
                                            <path d="M17.6994 6.71915C18.4527 6.71915 19.0633 6.10849 19.0633 5.35521C19.0633 4.60193 18.4527 3.99127 17.6994 3.99127C16.9461 3.99127 16.3354 4.60193 16.3354 5.35521C16.3354 6.10849 16.9461 6.71915 17.6994 6.71915Z" fill="white"/>
                                        </svg>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="phone-soc">
                            <div class="phone-wrapper">
                                <a href="tel:380672493154">+38(067) 249-31-54</a>
                                <a href="tel:380445947333">+38(044) 594-73-33</a>
                            </div>
                            <div class="line"></div>
                            <div class="soc-wrapper">
                                <a href="#">
                                    <svg width="11" height="22" viewBox="0 0 11 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0.322864 11.6827H2.51715V21.0775C2.51715 21.263 2.66165 21.4133 2.84001 21.4133H6.56045C6.73881 21.4133 6.88331 21.263 6.88331 21.0775V11.727H9.40582C9.56983 11.727 9.7078 11.599 9.72653 11.4295L10.1096 7.97083C10.1202 7.87568 10.0912 7.78038 10.03 7.70901C9.9687 7.63753 9.881 7.59663 9.78897 7.59663H6.88343V5.42858C6.88343 4.77499 7.22182 4.4436 7.88924 4.4436C7.98437 4.4436 9.78897 4.4436 9.78897 4.4436C9.96733 4.4436 10.1118 4.29324 10.1118 4.10778V0.933047C10.1118 0.747551 9.96733 0.597268 9.78897 0.597268H7.17087C7.15238 0.596333 7.11137 0.594788 7.05096 0.594788C6.59665 0.594788 5.01768 0.687535 3.77037 1.8809C2.38836 3.20336 2.58049 4.78674 2.62639 5.06129V7.59659H0.322864C0.144503 7.59659 0 7.74687 0 7.93237V11.3468C0 11.5323 0.144503 11.6827 0.322864 11.6827Z" fill="white"/>
                                    </svg>
                                </a>
                                <a href="#">
                                    <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M16.444 0H7.54852C3.93334 0 0.992188 2.94116 0.992188 6.55633V15.4518C0.992188 19.0669 3.93334 22.0081 7.54852 22.0081H16.444C20.0591 22.0081 23.0003 19.0669 23.0003 15.4518V6.55633C23.0003 2.94116 20.0591 0 16.444 0ZM20.7863 15.4518C20.7863 17.8499 18.8421 19.7941 16.444 19.7941H7.54852C5.15034 19.7941 3.2062 17.8499 3.2062 15.4518V6.55633C3.2062 4.15812 5.15034 2.21401 7.54852 2.21401H16.444C18.8421 2.21401 20.7863 4.15812 20.7863 6.55633V15.4518Z" fill="white"/>
                                        <path d="M11.9957 5.31232C8.85714 5.31232 6.30371 7.8661 6.30371 11.0051C6.30371 14.144 8.85714 16.6979 11.9957 16.6979C15.1343 16.6979 17.6877 14.1441 17.6877 11.0051C17.6877 7.86606 15.1343 5.31232 11.9957 5.31232ZM11.9957 14.4836C10.0749 14.4836 8.5177 12.9262 8.5177 11.0051C8.5177 9.08397 10.0749 7.52661 11.9957 7.52661C13.9166 7.52661 15.4738 9.08397 15.4738 11.0051C15.4738 12.9262 13.9166 14.4836 11.9957 14.4836Z" fill="white"/>
                                        <path d="M17.6994 6.71915C18.4527 6.71915 19.0633 6.10849 19.0633 5.35521C19.0633 4.60193 18.4527 3.99127 17.6994 3.99127C16.9461 3.99127 16.3354 4.60193 16.3354 5.35521C16.3354 6.10849 16.9461 6.71915 17.6994 6.71915Z" fill="white"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <h2>Спасибо!</h2>
                        <p>Наш менеджер свяжется с вами в ближайшее время</p>
                        <a href="index.php" class="button">вернуться на сайт</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>

<?php include ('components/popup.php');?>


<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 7]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
<![endif]-->
<!--[if lt IE 8]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="js/bootstrap.min.js"></script>

<script src="js/wow.min.js"></script>
<script src="js/likely.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/jquery.viewportchecker.min.js"></script>
<script src="js/js.js"></script>
</body>
</html>
