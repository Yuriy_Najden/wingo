<!DOCTYPE html>
<html lang="ru">
  <head>
   
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="" />
    <meta property="og:description" content="Доставка грузов «под ключ»" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="/img/og.jpg" />

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">

      <meta name="msapplication-TileColor" content="#001066">

      <meta name="theme-color" content="#001066">

    <title>Wingo</title>

      <link rel="stylesheet" href="css/animate.css">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
      <link type="text/css" rel="stylesheet" href="css/style.css"/>

  </head>
  <body>
  <div class="loader" id="loader"></div>
    <div class="wrapper">
        <section class="scroll-menu">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <div class="logo">
                            <img src="img/scroll-logo.svg" alt="">
                        </div>
                        <a class="mob-menu" id="mob-menu2">
                            <div class="top"></div>
                            <div class="center"></div>
                            <div class="bottom"></div>
                        </a>
                        <div class="mob-contacts">
                            <img src="img/scroll-logo.svg" alt="" class="menu-logo">
                            <div class="center-conteiner">
                                <h3>контакты</h3>
                                <div class="phone-wrapper">
                                    <a href="tel:380672493154">+38(067) 249-31-54</a>
                                    <a href="tel:380445947333">+38(044) 594-73-33</a>
                                </div>
                                <div class="soc-wrapper">
                                    <a href="#">
                                        <svg width="11" height="22" viewBox="0 0 11 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0.322864 11.6827H2.51715V21.0775C2.51715 21.263 2.66165 21.4133 2.84001 21.4133H6.56045C6.73881 21.4133 6.88331 21.263 6.88331 21.0775V11.727H9.40582C9.56983 11.727 9.7078 11.599 9.72653 11.4295L10.1096 7.97083C10.1202 7.87568 10.0912 7.78038 10.03 7.70901C9.9687 7.63753 9.881 7.59663 9.78897 7.59663H6.88343V5.42858C6.88343 4.77499 7.22182 4.4436 7.88924 4.4436C7.98437 4.4436 9.78897 4.4436 9.78897 4.4436C9.96733 4.4436 10.1118 4.29324 10.1118 4.10778V0.933047C10.1118 0.747551 9.96733 0.597268 9.78897 0.597268H7.17087C7.15238 0.596333 7.11137 0.594788 7.05096 0.594788C6.59665 0.594788 5.01768 0.687535 3.77037 1.8809C2.38836 3.20336 2.58049 4.78674 2.62639 5.06129V7.59659H0.322864C0.144503 7.59659 0 7.74687 0 7.93237V11.3468C0 11.5323 0.144503 11.6827 0.322864 11.6827Z" fill="white"/>
                                        </svg>
                                    </a>
                                    <a href="#">
                                        <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.444 0H7.54852C3.93334 0 0.992188 2.94116 0.992188 6.55633V15.4518C0.992188 19.0669 3.93334 22.0081 7.54852 22.0081H16.444C20.0591 22.0081 23.0003 19.0669 23.0003 15.4518V6.55633C23.0003 2.94116 20.0591 0 16.444 0ZM20.7863 15.4518C20.7863 17.8499 18.8421 19.7941 16.444 19.7941H7.54852C5.15034 19.7941 3.2062 17.8499 3.2062 15.4518V6.55633C3.2062 4.15812 5.15034 2.21401 7.54852 2.21401H16.444C18.8421 2.21401 20.7863 4.15812 20.7863 6.55633V15.4518Z" fill="white"/>
                                            <path d="M11.9957 5.31232C8.85714 5.31232 6.30371 7.8661 6.30371 11.0051C6.30371 14.144 8.85714 16.6979 11.9957 16.6979C15.1343 16.6979 17.6877 14.1441 17.6877 11.0051C17.6877 7.86606 15.1343 5.31232 11.9957 5.31232ZM11.9957 14.4836C10.0749 14.4836 8.5177 12.9262 8.5177 11.0051C8.5177 9.08397 10.0749 7.52661 11.9957 7.52661C13.9166 7.52661 15.4738 9.08397 15.4738 11.0051C15.4738 12.9262 13.9166 14.4836 11.9957 14.4836Z" fill="white"/>
                                            <path d="M17.6994 6.71915C18.4527 6.71915 19.0633 6.10849 19.0633 5.35521C19.0633 4.60193 18.4527 3.99127 17.6994 3.99127C16.9461 3.99127 16.3354 4.60193 16.3354 5.35521C16.3354 6.10849 16.9461 6.71915 17.6994 6.71915Z" fill="white"/>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <img src="img/mob-menu-bg-pic.svg" alt="" class="bg-pic">

                        </div>
                        <div class="phone-soc">
                            <div class="phone-wrapper">
                                <a href="tel:380672493154">+38(067) 249-31-54</a>
                                <a href="tel:380445947333">+38(044) 594-73-33</a>
                            </div>
                            <div class="line"></div>
                            <div class="soc-wrapper">
                                <a href="#">
                                    <svg width="11" height="22" viewBox="0 0 11 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0.322864 11.6827H2.51715V21.0775C2.51715 21.263 2.66165 21.4133 2.84001 21.4133H6.56045C6.73881 21.4133 6.88331 21.263 6.88331 21.0775V11.727H9.40582C9.56983 11.727 9.7078 11.599 9.72653 11.4295L10.1096 7.97083C10.1202 7.87568 10.0912 7.78038 10.03 7.70901C9.9687 7.63753 9.881 7.59663 9.78897 7.59663H6.88343V5.42858C6.88343 4.77499 7.22182 4.4436 7.88924 4.4436C7.98437 4.4436 9.78897 4.4436 9.78897 4.4436C9.96733 4.4436 10.1118 4.29324 10.1118 4.10778V0.933047C10.1118 0.747551 9.96733 0.597268 9.78897 0.597268H7.17087C7.15238 0.596333 7.11137 0.594788 7.05096 0.594788C6.59665 0.594788 5.01768 0.687535 3.77037 1.8809C2.38836 3.20336 2.58049 4.78674 2.62639 5.06129V7.59659H0.322864C0.144503 7.59659 0 7.74687 0 7.93237V11.3468C0 11.5323 0.144503 11.6827 0.322864 11.6827Z" fill="white"/>
                                    </svg>
                                </a>
                                <a href="#">
                                    <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M16.444 0H7.54852C3.93334 0 0.992188 2.94116 0.992188 6.55633V15.4518C0.992188 19.0669 3.93334 22.0081 7.54852 22.0081H16.444C20.0591 22.0081 23.0003 19.0669 23.0003 15.4518V6.55633C23.0003 2.94116 20.0591 0 16.444 0ZM20.7863 15.4518C20.7863 17.8499 18.8421 19.7941 16.444 19.7941H7.54852C5.15034 19.7941 3.2062 17.8499 3.2062 15.4518V6.55633C3.2062 4.15812 5.15034 2.21401 7.54852 2.21401H16.444C18.8421 2.21401 20.7863 4.15812 20.7863 6.55633V15.4518Z" fill="white"/>
                                        <path d="M11.9957 5.31232C8.85714 5.31232 6.30371 7.8661 6.30371 11.0051C6.30371 14.144 8.85714 16.6979 11.9957 16.6979C15.1343 16.6979 17.6877 14.1441 17.6877 11.0051C17.6877 7.86606 15.1343 5.31232 11.9957 5.31232ZM11.9957 14.4836C10.0749 14.4836 8.5177 12.9262 8.5177 11.0051C8.5177 9.08397 10.0749 7.52661 11.9957 7.52661C13.9166 7.52661 15.4738 9.08397 15.4738 11.0051C15.4738 12.9262 13.9166 14.4836 11.9957 14.4836Z" fill="white"/>
                                        <path d="M17.6994 6.71915C18.4527 6.71915 19.0633 6.10849 19.0633 5.35521C19.0633 4.60193 18.4527 3.99127 17.6994 3.99127C16.9461 3.99127 16.3354 4.60193 16.3354 5.35521C16.3354 6.10849 16.9461 6.71915 17.6994 6.71915Z" fill="white"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <header>
            <div class="top-line">
                <div class="container-fluid">
                    <div class="row">
                        <div class="content col-12">
                            <div class="logo">
                                <img src="img/logo.svg" alt="">
                            </div>
                            <a class="mob-menu" id="mob-menu">
                                <div class="top"></div>
                                <div class="center"></div>
                                <div class="bottom"></div>
                            </a>
                            <div class="mob-contacts">
                                <img src="img/scroll-logo.svg" alt="" class="menu-logo">
                                <div class="center-conteiner">
                                    <h3>контакты</h3>
                                    <div class="phone-wrapper">
                                        <a href="tel:380672493154">+38(067) 249-31-54</a>
                                        <a href="tel:380445947333">+38(044) 594-73-33</a>
                                    </div>
                                    <div class="soc-wrapper">
                                        <a href="#">
                                            <svg width="11" height="22" viewBox="0 0 11 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M0.322864 11.6827H2.51715V21.0775C2.51715 21.263 2.66165 21.4133 2.84001 21.4133H6.56045C6.73881 21.4133 6.88331 21.263 6.88331 21.0775V11.727H9.40582C9.56983 11.727 9.7078 11.599 9.72653 11.4295L10.1096 7.97083C10.1202 7.87568 10.0912 7.78038 10.03 7.70901C9.9687 7.63753 9.881 7.59663 9.78897 7.59663H6.88343V5.42858C6.88343 4.77499 7.22182 4.4436 7.88924 4.4436C7.98437 4.4436 9.78897 4.4436 9.78897 4.4436C9.96733 4.4436 10.1118 4.29324 10.1118 4.10778V0.933047C10.1118 0.747551 9.96733 0.597268 9.78897 0.597268H7.17087C7.15238 0.596333 7.11137 0.594788 7.05096 0.594788C6.59665 0.594788 5.01768 0.687535 3.77037 1.8809C2.38836 3.20336 2.58049 4.78674 2.62639 5.06129V7.59659H0.322864C0.144503 7.59659 0 7.74687 0 7.93237V11.3468C0 11.5323 0.144503 11.6827 0.322864 11.6827Z" fill="white"/>
                                            </svg>
                                        </a>
                                        <a href="#">
                                            <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M16.444 0H7.54852C3.93334 0 0.992188 2.94116 0.992188 6.55633V15.4518C0.992188 19.0669 3.93334 22.0081 7.54852 22.0081H16.444C20.0591 22.0081 23.0003 19.0669 23.0003 15.4518V6.55633C23.0003 2.94116 20.0591 0 16.444 0ZM20.7863 15.4518C20.7863 17.8499 18.8421 19.7941 16.444 19.7941H7.54852C5.15034 19.7941 3.2062 17.8499 3.2062 15.4518V6.55633C3.2062 4.15812 5.15034 2.21401 7.54852 2.21401H16.444C18.8421 2.21401 20.7863 4.15812 20.7863 6.55633V15.4518Z" fill="white"/>
                                                <path d="M11.9957 5.31232C8.85714 5.31232 6.30371 7.8661 6.30371 11.0051C6.30371 14.144 8.85714 16.6979 11.9957 16.6979C15.1343 16.6979 17.6877 14.1441 17.6877 11.0051C17.6877 7.86606 15.1343 5.31232 11.9957 5.31232ZM11.9957 14.4836C10.0749 14.4836 8.5177 12.9262 8.5177 11.0051C8.5177 9.08397 10.0749 7.52661 11.9957 7.52661C13.9166 7.52661 15.4738 9.08397 15.4738 11.0051C15.4738 12.9262 13.9166 14.4836 11.9957 14.4836Z" fill="white"/>
                                                <path d="M17.6994 6.71915C18.4527 6.71915 19.0633 6.10849 19.0633 5.35521C19.0633 4.60193 18.4527 3.99127 17.6994 3.99127C16.9461 3.99127 16.3354 4.60193 16.3354 5.35521C16.3354 6.10849 16.9461 6.71915 17.6994 6.71915Z" fill="white"/>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                                <img src="img/mob-menu-bg-pic.svg" alt="" class="bg-pic">
                            </div>
                            <div class="phone-soc">
                                <div class="phone-wrapper">
                                    <a href="tel:380672493154">+38(067) 249-31-54</a>
                                    <a href="tel:380445947333">+38(044) 594-73-33</a>
                                </div>
                                <div class="line"></div>
                                <div class="soc-wrapper">
                                    <a href="#">
                                        <svg width="11" height="22" viewBox="0 0 11 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0.322864 11.6827H2.51715V21.0775C2.51715 21.263 2.66165 21.4133 2.84001 21.4133H6.56045C6.73881 21.4133 6.88331 21.263 6.88331 21.0775V11.727H9.40582C9.56983 11.727 9.7078 11.599 9.72653 11.4295L10.1096 7.97083C10.1202 7.87568 10.0912 7.78038 10.03 7.70901C9.9687 7.63753 9.881 7.59663 9.78897 7.59663H6.88343V5.42858C6.88343 4.77499 7.22182 4.4436 7.88924 4.4436C7.98437 4.4436 9.78897 4.4436 9.78897 4.4436C9.96733 4.4436 10.1118 4.29324 10.1118 4.10778V0.933047C10.1118 0.747551 9.96733 0.597268 9.78897 0.597268H7.17087C7.15238 0.596333 7.11137 0.594788 7.05096 0.594788C6.59665 0.594788 5.01768 0.687535 3.77037 1.8809C2.38836 3.20336 2.58049 4.78674 2.62639 5.06129V7.59659H0.322864C0.144503 7.59659 0 7.74687 0 7.93237V11.3468C0 11.5323 0.144503 11.6827 0.322864 11.6827Z" fill="white"/>
                                        </svg>
                                    </a>
                                    <a href="#">
                                        <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.444 0H7.54852C3.93334 0 0.992188 2.94116 0.992188 6.55633V15.4518C0.992188 19.0669 3.93334 22.0081 7.54852 22.0081H16.444C20.0591 22.0081 23.0003 19.0669 23.0003 15.4518V6.55633C23.0003 2.94116 20.0591 0 16.444 0ZM20.7863 15.4518C20.7863 17.8499 18.8421 19.7941 16.444 19.7941H7.54852C5.15034 19.7941 3.2062 17.8499 3.2062 15.4518V6.55633C3.2062 4.15812 5.15034 2.21401 7.54852 2.21401H16.444C18.8421 2.21401 20.7863 4.15812 20.7863 6.55633V15.4518Z" fill="white"/>
                                            <path d="M11.9957 5.31232C8.85714 5.31232 6.30371 7.8661 6.30371 11.0051C6.30371 14.144 8.85714 16.6979 11.9957 16.6979C15.1343 16.6979 17.6877 14.1441 17.6877 11.0051C17.6877 7.86606 15.1343 5.31232 11.9957 5.31232ZM11.9957 14.4836C10.0749 14.4836 8.5177 12.9262 8.5177 11.0051C8.5177 9.08397 10.0749 7.52661 11.9957 7.52661C13.9166 7.52661 15.4738 9.08397 15.4738 11.0051C15.4738 12.9262 13.9166 14.4836 11.9957 14.4836Z" fill="white"/>
                                            <path d="M17.6994 6.71915C18.4527 6.71915 19.0633 6.10849 19.0633 5.35521C19.0633 4.60193 18.4527 3.99127 17.6994 3.99127C16.9461 3.99127 16.3354 4.60193 16.3354 5.35521C16.3354 6.10849 16.9461 6.71915 17.6994 6.71915Z" fill="white"/>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="header-slider">
                <div class="container-fluid">
                    <div class="row">
                        <div class="content col-12">
                            <div class="slider-body" id="header-slider">
                                <div class="slide">
                                    <img src="img/header-bg-1.jpg" alt="">
                                    <div>
                                        <h1>Доставка грузов <br> «под ключ»</h1>
                                        <p>Экспресс-доставка, авиа, море, Ж/Д</p>
                                        <a href="#contact-form" class="button scroll-to">Бесплатная консультация</a>
                                    </div>

                                </div>
                                <div class="slide">
                                    <img src="img/header-bg-2.jpg" alt="">
                                    <div>
                                        <h1>Экспресс-доставка грузов <br> за 3-5 дней</h1>
                                        <p>От двери до двери, включая таможенную очистку</p>
                                        <a href="#contact-form" class="button scroll-to">Бесплатная консультация</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="word-map">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <div class="text">
                            <h2 class="block-title">экспресс-доставка грузов</h2>
                            <p>Эксклюзивный сервис доставки на рынке Украины за рекордно короткий срок из любой точки мира.</p>
                            <div class="items-wrapper">
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                    <img src="img/usa-flag.svg" alt="">
                                    <p>США</p>
                                </div>
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                                    <img src="img/brazil-flag.svg" alt="">
                                    <p>Бразилия</p>
                                </div>
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">
                                    <img src="img/china-flag.svg" alt="">
                                    <p>Китай</p>
                                </div>
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.4s">
                                    <img src="img/korea-flag.svg" alt="">
                                    <p>Южная Корея</p>
                                </div>
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.7s">
                                    <img src="img/kanada-flag.svg" alt="">
                                    <p>Канада</p>
                                </div>
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="2s">
                                    <img src="img/izrail-flag.svg" alt="">
                                    <p>Израиль</p>
                                </div>
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="2.3s">
                                    <img src="img/japan-flag.svg" alt="">
                                    <p>Япония</p>
                                </div>
                                <div class="item wow fadeIn" data-wow-duration="1s" data-wow-delay="2.6s">
                                    <img src="img/india-flag.svg" alt="">
                                    <p>Индия</p>
                                </div>
                            </div>
                            <a href="#" class="more-cantris wow fadeIn" data-wow-duration="1s" data-wow-delay="2.9s" data-toggle="modal" data-target="#cantryModal">+200 стран</a>
                        </div>
                        <div class="pic">
                            <img src="img/map.png" alt="" class="word-map-static">
                            <img src="img/word-map-animate.png" class="animation-map" id="animate-map" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="range-services">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12" id="start-paralax">
                        <div class="name-button">
                            <div class="name">
                                <h2 class="block-title">Полный комплекс услуг</h2>
                                <p>Доверяя свой груз нам, вы получаете высокий уровень сервиса на каждом этапе доставки</p>
                            </div>
                            <a href="calc.php" class="button desc-btn">Рассчитать стоимость</a>
                        </div>

                        <div class="items-wrapper">
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                <div class="day">
                                    <p>01.</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/complex-pic-1.jpg" alt="">
                                        </div>

                                        <p>Поиск поставщика</p>
                                    </div>
                                </div>
                            </div>
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                                <div class="day">
                                    <p>02.</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/complex-pic-2.jpg" alt="">
                                        </div>

                                        <p>Выкуп товара</p>
                                    </div>
                                </div>
                            </div>
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">
                                <div class="day">
                                    <p>03.</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/complex-pic-3.jpg" alt="">
                                        </div>

                                        <p>Экспортное оформление</p>
                                    </div>
                                </div>
                            </div>
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="1.4s">
                                <div class="day">
                                    <p>04.</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/complex-pic-4.jpg" alt="">
                                        </div>

                                        <p>Доставка в Украину</p>
                                    </div>
                                </div>
                            </div>
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="1.7s">
                                <div class="day">
                                    <p>05.</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/complex-pic-5.jpg" alt="">
                                        </div>

                                        <p>Таможенная очистка</p>
                                    </div>
                                </div>
                            </div>
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="2s">
                                <div class="day">
                                    <p>06.</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/complex-pic-6.jpg" alt="">
                                        </div>

                                        <p>Доставка получателю</p>
                                    </div>
                                </div>
                            </div>
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="2.3s">
                                <div class="day">
                                    <p>07.</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/complex-pic-7.jpg" alt="">
                                        </div>
                                        <p>Постановка товара на баланс</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="calc.php" class="button mob-btn">Рассчитать стоимость</a>
                    </div>
                </div>
                <div class="row">
                    <div class="parallax col-12">
                        <img src="img/container-pic.png" alt="" class="wow lightSpeedIn"data-wow-offset="100" data-wow-duration="1s" data-wow-delay="0.5s">
                    </div>
                </div>
            </div>
        </section>

        <section class="delivery-methods">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <h2 class="block-title">способы доставки</h2>
                        <div class="method">
                            <img src="img/airplane.png" alt="">
                            <div class="text">
                                <h3>авиа</h3>
                                <p>от 300 кг</p>
                                <p>7-14 дней EXW</p>
                                <p>от 6 $/кг</p>
                            </div>
                        </div>
                        <div class="method">
                            <img src="img/ship.png" alt="">
                            <div class="text">
                                <h3>Море</h3>
                                <p>от 1 м³</p>
                                <p>45-50 дней FoB</p>
                                <p>От 250 $/м³</p>
                            </div>
                        </div>
                        <div class="method">
                            <img src="img/train.png" alt="">
                            <div class="text">
                                <h3>Ж/д</h3>
                                <p>от 500 кг</p>
                                <p>25-30 дней FoB</p>
                                <p>от 2 $/кг</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="express-delivery">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <div class="name-button">
                            <div class="name">
                                <h2 class="block-title">Экспресс-доставка</h2>
                                <p>Самая быстрая доставка грузов «под ключ» из стран дальнего зарубежья</p>
                            </div>
                            <a href="calc.php" class="button desc-btn">Рассчитать стоимость</a>
                        </div>

                        <div class="items-wrapper">
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                                <div class="day">
                                    <p>01 день</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/expres-step-pic-1.jpg" alt="">
                                        </div>

                                        <p>Подготовка документов</p>
                                    </div>
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/expres-step-pic-2.jpg" alt="">
                                        </div>

                                        <p>Забор груза у отправителя</p>
                                    </div>
                                </div>
                            </div>
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                                <div class="day">
                                    <p>02-03 день</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/expres-step-pic-3.jpg" alt="">
                                        </div>

                                        <p>Авиа-доставка в Украину</p>
                                    </div>
                                </div>
                            </div>
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">
                                <div class="day">
                                    <p>04 день</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/expres-step-pic-4.jpg" alt="">
                                        </div>

                                        <p>Таможенная очистка</p>
                                    </div>
                                </div>
                            </div>
                            <div class="items wow fadeIn" data-wow-duration="1s" data-wow-delay="1.4s">
                                <div class="day">
                                    <p>05 день</p>
                                    <hr>
                                    <div class="mob-line"></div>
                                </div>
                                <div class="step-wrapper">
                                    <div class="step">
                                        <div class="mob-pic-wrapper">
                                            <img src="img/expres-step-pic-5.jpg" alt="">
                                        </div>

                                        <p>Выдача груза получателю</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="calc.php" class="button mob-btn">Рассчитать стоимость</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="disruption">
            <marquee behavior="scroll" direction="right" loop="-1" scrollamount="10" class="top-pic" >
                <img src="img/bottom-arrow-line.png" alt="" >
                <img src="img/bottom-arrow-line.png" alt="" >
                <img src="img/bottom-arrow-line.png" alt="" >
                <img src="img/bottom-arrow-line.png" alt="" >
                <img src="img/bottom-arrow-line.png" alt="" >
                <img src="img/bottom-arrow-line.png" alt="" >
            </marquee>
            <marquee behavior="scroll" direction="left" loop="-1" scrollamount="10" class="bottom-pic">
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
            </marquee>


            <div class="container-fluid">
                <div class="row">
                    <div class="content col-md-12">
                        <h2>При нарушении сроков экспресс-доставки, доставка груза — <span>в подарок</span></h2>
                        <a href="#contact-form" class="button scroll-to">заказать экспресс-доставку</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="advantages">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-md-12">
                        <div class="item-title">
                            <h2 class="block-title">Основные преимущества экспресс-доставки для бизнеса</h2>
                            <div class="pic-wrapper">
                                <img src="img/prime-pic-1.svg" alt="">
                                <img src="img/prime-pic-2.svg" alt="">
                                <img src="img/prime-pic-3.svg" alt="">
                            </div>
                        </div>
                        <div class="item-list">
                            <div class="item">
                                <p class="number">01.</p>
                                <div class="text">
                                    <h3>Повышение оборачиваемости собственных денежных средств</h3>
                                    <p>Воспользовавшись экспресс-доставкой, вы сможете быстро привезти и продать товар,
                                        и снова пустить деньги в оборот. В итоге, вы получаее в два раза больше прыбыли
                                        при тех же объемах и бюджете.
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <p class="number">02.</p>
                                <div class="text">
                                    <h3>Снижение размера издержек на поддержание завышенных складских запасов в Украине</h3>
                                    <p>Закупая большие партии товара, вы тратите свои деньги на содержание склада.
                                        Повышается риск, что пока товар лежит на складе, он утратит свою актуальность
                                        и значительно упадет в цене.
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <p class="number">03.</p>
                                <div class="text">
                                    <h3>Повышение продажи за счет постоянного обновления ассортимента</h3>
                                    <p>Отправляя свои грузы экспресс-доставкой, вы можете обновлять свой ассортимент
                                        минимум раз в неделю и получать дополнительную прибыль на продаже новинок и
                                        трендов.
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <p class="number">04.</p>
                                <div class="text">
                                    <h3>Экономия времени и затрат на перевозку</h3>
                                    <p>При перевозке груза до 300 кг экспресс-доставкой вы экономите до 50% времении
                                        и затрат по сравнению со стандатной авиаперевозкой и до 35% — с эконом вариантом.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="popular-questions">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <h2 class="block-title">Популярные вопросы об экспресс-доставке Wingo</h2>
                        <div class="accordion-wrapper" id="accordion">

                            <div class="card">
                                <div class="card-header">
                                    <a class="card-link" data-toggle="collapse" href="#questions1">
                                        За счет чего получается такой быстрый срок доставки?
                                    </a>
                                </div>
                                <div id="questions1" class="collapse show" data-parent="#accordion">
                                    <div class="card-body">
                                        Мы имеем партнерскую курьерскую сеть по всему миру, что позволяет забирать груз у
                                        отправителя день-в-день. Вылеты — ежедневно из всех крупных аэропортов.
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#questions2">
                                        Чем экспресс-доставка Wingo отличается от услуг других компаний?
                                    </a>
                                </div>
                                <div id="questions2" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        Мы единствинные в Украине, кто гарантирует вам рекордно быструю доставку от 3 до
                                        5 дней. За этот срок мы заберем груз от двери поставщика, подготовим документы,
                                        доставим груз в Киев, проведем таможенную очистку и доставим вам до двери.
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#questions3">
                                        Из каких стран возможна доставка?
                                    </a>
                                </div>
                                <div id="questions3" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        У нас есть возможность доставлять грузы данным сервисом из более чем из 200 стран мира.
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#questions4">
                                        Нужно ли растамаживать груз и оплачивать таможенные платежи при получении груза в Украину?
                                    </a>
                                </div>
                                <div id="questions4" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        С доставкой «под ключ» мы берем на себя все работы по таможенной очистке и оплате
                                        таможенных платежей. Тариф, который мы озвучиваем, включает все работы и платежи.
                                        Вам ни о чем не придется беспокоится.
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <a class="collapsed card-link" data-toggle="collapse" href="#questions5">
                                        Какая стоимость доставки и как она рассчитывается?
                                    </a>
                                </div>
                                <div id="questions5" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        Стоимость доставки зависит от веса груза и его стоимости. Минимальный тариф при
                                        весе груза более 60 кг. У нас вы всегда можете получить бесплатную консультацию
                                        по своему грузу.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="reliable-partner">
            <marquee behavior="scroll" direction="left" loop="-1" scrollamount="10" class="top-pic">
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
                <img src="img/top-arrow-line.png" alt="" >
            </marquee>

            <div class="container-fluid">
                <div class="row">
                    <div class="content col-xl-10 offset-xl-1 col-12 offset-0">
                        <div class="items-wrapper">
                            <div class="item item-info">
                                <h2 class="block-title">Wingo — ваш надежный партнер</h2>
                                <p>Эксперт в отрасли грузоперевозок</p>
                                <a href="#" class="button">скачать презентацию</a>
                                <div class="pic-wrapper">
                                    <img src="img/partner-pic-1.svg" alt="">
                                    <img src="img/partner-pic-2.svg" alt="">
                                    <img src="img/partner-pic-3.svg" alt="">
                                </div>
                            </div>
                            <div class="item item-text">
                                <p>Компания Wingo с 2009 года предоставляет полный комплекс услуг по сопровождению импортных
                                    поставок «под ключ» от двери до двери. Сотрудничая с нами, клиенту не нужно контролировать
                                    каждый этап доставки и подключать к выполнению задач разные компании.
                                </p>
                                <p>Доверив нам свой груз, мы выполним полный комплекс услуг, начиная от переговоров с
                                    поставщиками, заканчивая доставкой груза на склад клиента и оформлением документов для
                                    постановки товара на баланс компании клиента.
                                </p>
                                <p>Наша цель — полностью оградить вас от организационных и логистических решений, связанных
                                    с импортом груза. Дать возможность вашему бизнесу максимально сосредоточиться на
                                    маркетинге продукта, а не логистике.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="reviews">
            <div class="content">
                <h2 class="block-title">Отзывы наших клиентов</h2>
                <div class="controls-wrapper">
                    <a href="#" class="control prev">
                        <svg width="17" height="27" viewBox="0 0 17 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15 2L3 13.5L15 25" stroke="black" stroke-width="3"/>
                        </svg>
                    </a>
                    <p>/8</p>
                    <a href="#" class="control next">
                        <svg width="17" height="27" viewBox="0 0 17 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M2 2L14 13.5L2 25" stroke="black" stroke-width="3"/>
                        </svg>
                    </a>
                </div>
                <div class="rev-slider" id="rev-slider">
                    <div class="slide">
                        <p class="slide-number">1</p>
                        <div class="slide-content">
                            <div class="triangle"></div>
                            <div class="photo">
                                <p>к</p>
                            </div>
                            <div class="text">
                                <h3>Катерина</h3>
                                <p>Имея за плечами трехлетний опыт работы с различными компаниями доставки грузов, я бесконечно
                                    рада, что открыла для себя компанию \"Wingo\". Это замечательные ребята, настоящие профессионалы.
                                    Цены компании не выше среднерыночных, зато сервис выше всяких похвал, и, самое главное,
                                    срок доставки - это действительно 3-5 дней! Плюс индивидуальный подход к каждому клиенту.
                                    \"Wingo\" спасибо, вы лучшие!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <p class="slide-number">2</p>
                        <div class="slide-content">
                            <div class="triangle"></div>
                            <div class="photo">
                                <p>а</p>
                            </div>
                            <div class="text">
                                <h3>Анатолий</h3>
                                <p>Я сотрудничаю с компанией "Wingo" больше года и могу сказать, что это порядочные, интеллигентные
                                    и ответственные люди. И если за время нашей совместной работы пару раз и случались
                                    какие-то накладки (задержки с поставкой или поставка не совсем того товара - причем
                                    не по вине нашей стороны), то сотрудники компании всегда старались свести мои неудобства
                                    и потери до минимума (в том числе принимая часть убытков на себя). А это на сегодня
                                    редкое явление и дорого стоит...
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <p class="slide-number">3</p>
                        <div class="slide-content">
                            <div class="triangle"></div>
                            <div class="photo">
                                <p>и</p>
                            </div>
                            <div class="text">
                                <h3>Игорь</h3>
                                <p>Огромным преимуществом компании являются сроки доставки. Быстрее не делает никто. На
                                    практике, действительно 3-5 дней. Бывают организационные вопросы, но они решаемые.
                                    Цены не самые дешевые, но в пределах рыночных.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <p class="slide-number">4</p>
                        <div class="slide-content">
                            <div class="triangle"></div>
                            <div class="photo">
                                <p>д</p>
                            </div>
                            <div class="text">
                                <h3>Дмитрий</h3>
                                <p>Выражаю огромную благодарность компании WINGO за высокий профессионализм, оперативность
                                    и качественное выполнение своих обязанностей. Все доставки грузов, выполняются точно
                                    в поставленные сроки. Успехов Вашей компании и процветания. Спасибо за сотрудничество.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <p class="slide-number">5</p>
                        <div class="slide-content">
                            <div class="triangle"></div>
                            <div class="photo">
                                <p>ае</p>
                            </div>
                            <div class="text">
                                <h3>Антон Е</h3>
                                <p>Пишу на правах действующего клиента! На самом деле сервис на высоком уровне, цены хорошие.
                                    Если учитывать личный опыт в данном сегменте рынка, то условия работы очень конкурентноспособны
                                    и не многие могут предложить что-то подобное!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <p class="slide-number">6</p>
                        <div class="slide-content">
                            <div class="triangle"></div>
                            <div class="photo">
                                <p>с</p>
                            </div>
                            <div class="text">
                                <h3>Сергей</h3>
                                <p>Отличная работа! Просто нет слов, срок доставки как был и обещан, весь груз дошел отлично,
                                    нареканий нет. Побольше бы таких компаний которые так относятся к своим клиентам!!!
                                    Рекомендую!!!!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <p class="slide-number">7</p>
                        <div class="slide-content">
                            <div class="triangle"></div>
                            <div class="photo">
                                <p>см</p>
                            </div>
                            <div class="text">
                                <h3>Сергей Макаренко</h3>
                                <p>Опыт работы неплохой (немного напрягло несколько переключений между менеджерами, но
                                    возможно так не всегда)). Огромнейший позитив в том, что ребята предлагают по разным
                                    товарам не только Китай, но и Индию, США. Сработали по альтернативе на 5. Также приятно,
                                    что компания развивает новые идеи и всегда что-то интересное есть! молодцы!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <p class="slide-number">8</p>
                        <div class="slide-content">
                            <div class="triangle"></div>
                            <div class="photo">
                                <p>д</p>
                            </div>
                            <div class="text">
                                <h3>Дмитрий</h3>
                                <p>Приятно работать с профессионалами, которые выполняют свою работу отлично. Всё быстро
                                    и хорошо, теперь хоть есть гарантии доставки груза в сроки, а то связался до этого с
                                    другими и ждали товар полтора месяца. Спасибо!
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="customer-benefits">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <h2 class="block-title">Преимущества компании для клиентов</h2>
                        <div class="items-wrapper">
                            <div class="item">
                                <svg width="120" height="119" viewBox="0 0 120 119" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect x="1.5" y="1.5" width="117" height="116" stroke="black" stroke-width="3"/>
                                    <path d="M42.6494 64.8894C41.4665 65.6801 40.0207 66.0754 38.3121 66.0754C35.092 66.0754 32.4962 65.12 30.5247 63.2092C28.6189 61.2326 27.666 57.7734 27.666 52.8317V40.8728C27.666 38.6326 27.9617 36.557 28.5532 34.6463C29.2104 32.7355 30.1632 31.0882 31.4119 29.7046C32.6605 28.255 34.2377 27.1349 36.1434 26.3442C38.0492 25.5535 40.3164 25.1582 42.9451 25.1582C47.8081 25.1582 51.4882 26.443 53.9854 29.0127C56.4827 31.5824 57.7313 35.0416 57.7313 39.3903V48.1865C57.7313 53.3917 57.0741 57.8393 55.7598 61.5291L43.1422 94.5396H31.9047L43.0437 65.2848L42.6494 64.8894ZM38.2135 53.3259C38.2135 54.6436 38.6407 55.6649 39.495 56.3897C40.3493 57.1145 41.4336 57.4769 42.7479 57.4769C44.2594 57.4769 45.3766 56.9827 46.0995 55.9944C46.8224 54.9401 47.1838 53.7541 47.1838 52.4364V39.1926C47.1838 37.8748 46.7566 36.8865 45.9023 36.2276C45.1137 35.5028 44.0623 35.1404 42.7479 35.1404C41.4993 35.1404 40.415 35.5028 39.495 36.2276C38.6407 36.8865 38.2135 37.8748 38.2135 39.1926V53.3259Z" fill="black"/>
                                    <path d="M82.8979 59.9689V70.8346H77.0854V59.9689H66.9827V54.0169H77.0854V43.082H82.8979V54.0169H93.0007V59.9689H82.8979Z" fill="black"/>
                                </svg>
                                <p>Успешный опыт работы более 9 лет</p>
                            </div>
                            <div class="item">
                                <svg width="120" height="119" viewBox="0 0 120 119" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect x="1.5" y="1.5" width="117" height="116" stroke="black" stroke-width="3"/>
                                    <path d="M86.7934 67.6018C87.6173 70.948 78.6028 69.8864 76.4012 73.0192C74.1981 76.1505 68.8155 74.0465 67.5674 73.4446C66.3193 72.8426 61.539 74.8488 61.9569 71.4384C62.3722 68.0271 65.2803 67.8268 68.3991 66.4226C71.5164 65.0199 76.0128 60.3558 73.8034 59.7462C63.4009 56.8788 59.852 46.8365 59.852 46.8365C59.2279 46.7159 59.3846 39.7374 56.6837 39.537C53.9803 39.3357 52.5245 40.3393 48.577 39.537C44.627 38.7348 45.4597 36.1267 44.8356 30.5089C44.2131 24.8911 46.5001 26.697 47.7354 24.8922C47.8434 24.7337 47.9839 24.5924 48.1213 24.4512C33.6578 29.5402 23.2258 43.6422 23.2258 60.2079C23.2258 60.2342 23.2273 60.2604 23.2273 60.2866C25.9155 60.4647 30.8106 61.048 31.0409 62.9431C31.359 65.5603 31.0409 68.8319 33.2695 69.8133C35.4981 70.7946 36.1349 65.8873 38.0448 67.1961C39.9547 68.5049 45.0487 69.7648 45.0487 72.0787C45.0487 74.3921 44.0938 77.9922 45.0487 78.6466C46.0037 79.301 49.8216 83.8809 49.8226 84.5353C49.8241 85.1898 51.1684 88.5173 50.7673 89.7893C50.4349 90.8439 48.7577 94.1185 47.1315 95.6074C51.1389 97.1504 55.4739 97.9996 59.9993 97.9996C80.2771 97.9996 96.7742 81.0463 96.7742 60.2079C96.7742 54.0366 95.3175 48.2105 92.754 43.0599C93 46.9031 92.834 50.4053 93.653 51.5527C97.2367 56.5675 84.9228 60.0172 86.7934 67.6018Z" fill="black"/>
                                    <circle cx="60" cy="60" r="36.5" stroke="black" stroke-width="3"/>
                                </svg>

                                <p>Склады в Китае, Кореи, США и других странах</p>
                            </div>
                            <div class="item">
                                <svg width="120" height="119" viewBox="0 0 120 119" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect x="1.5" y="1.5" width="117" height="116" stroke="black" stroke-width="3"/>
                                    <rect x="31.5" y="26.5" width="57" height="67" stroke="black" stroke-width="3"/>
                                    <path d="M47 38H73" stroke="black" stroke-width="3"/>
                                    <path d="M39 53H81" stroke="black" stroke-width="3"/>
                                    <path d="M39 60H81" stroke="black" stroke-width="3"/>
                                    <path d="M39 67H81" stroke="black" stroke-width="3"/>
                                    <circle cx="74" cy="82" r="7" fill="black"/>
                                </svg>

                                <p>Эксклюзивные контракты с ведущими авиа и морскими линиями</p>
                            </div>
                            <div class="item">
                                <svg width="120" height="119" viewBox="0 0 120 119" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect x="1.5" y="1.5" width="117" height="116" stroke="black" stroke-width="3"/>
                                    <path d="M55 56.5L50 47.5L55 43H63.5L68.5 47.5L63.5 56.5L68 87L59 96.5L50 87L55 56.5Z" fill="black"/>
                                    <path d="M44.5 52.5L55 43L35 27.5L27 33.5L44.5 52.5Z" stroke="black" stroke-width="3"/>
                                    <path d="M74.5 52.5L64 43L84 27.5L92 33.5L74.5 52.5Z" stroke="black" stroke-width="3"/>
                                    <path d="M53 57H65" stroke="white" stroke-width="3"/>
                                </svg>

                                <p>Внутренний штат таможенных брокеров, логистов и специалистов ВЭД</p>
                            </div>
                            <div class="item">
                                <svg width="120" height="119" viewBox="0 0 120 119" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect x="1.5" y="1.5" width="117" height="116" stroke="black" stroke-width="3"/>
                                    <path d="M39.7752 90C35.3487 90 32.0989 88.7179 30.0257 86.1538C28.0086 83.5897 27 80.0285 27 75.4701L35.7409 74.0171V75.2137C35.7409 77.4359 36.0771 79.0313 36.7495 80C37.4218 80.9117 38.4864 81.3675 39.9433 81.3675C40.9518 81.3675 41.8203 81.0256 42.5487 80.3419C43.3332 79.6581 43.7814 78.3476 43.8935 76.4103V62.3077C43.8935 59.8575 42.6888 58.6325 40.2795 58.6325C39.3829 58.6325 38.5145 58.8889 37.674 59.4017C36.8895 59.8575 36.2732 60.6553 35.825 61.7949H28.2607V30H52.6344V38.4615H36.4973V52.8205C36.5534 52.8205 36.5814 52.849 36.5814 52.906C36.6374 52.906 36.7215 52.906 36.8335 52.906C37.5619 52.1083 38.4864 51.5385 39.6071 51.1966C40.7837 50.7977 41.9044 50.5983 42.969 50.5983C46.555 50.5983 49.1044 51.6239 50.6173 53.6752C52.1861 55.6695 52.9706 58.2051 52.9706 61.2821V73.5897C52.9706 79.2308 51.878 83.3903 49.6927 86.0684C47.5635 88.6895 44.2577 90 39.7752 90Z" fill="black"/>
                                    <path d="M83.7548 89.4872L74.3415 63.8462C73.7252 65.2137 73.0808 66.6097 72.4084 68.0342C71.7921 69.4017 71.1477 70.7692 70.4754 72.1368V89.4872H61.4823V30H70.4754V54.0171H70.8956L81.9058 30H91.3191C89.358 34.0456 87.3969 38.0627 85.4358 42.0513C83.4747 46.0399 81.5136 50.057 79.5525 54.1026L93 89.4872H83.7548Z" fill="black"/>
                                </svg>

                                <p>Доверие более 5000 клиентов</p>
                            </div>
                            <div class="item">
                                <svg width="120" height="119" viewBox="0 0 120 119" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect x="1.5" y="1.5" width="117" height="116" stroke="black" stroke-width="3"/>
                                    <path d="M31 51.5046V31.5L59.5 23L88.5 31.5V51.2451C88.5 67.6841 80.4194 83.0728 66.8867 92.4058L59.5 97.5L52.2787 92.4324C38.9404 83.0722 31 67.7995 31 51.5046Z" stroke="black" stroke-width="3"/>
                                    <path d="M54.5 54V48.5C54.5 45.4624 56.9624 43 60 43V43C63.0376 43 65.5 45.4624 65.5 48.5V54" stroke="black" stroke-width="3"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M71 54H49V72H71V54ZM62 61C62 61.7403 61.5978 62.3866 61 62.7324V66H59V62.7324C58.4022 62.3866 58 61.7403 58 61C58 59.8954 58.8954 59 60 59C61.1046 59 62 59.8954 62 61Z" fill="black"/>
                                </svg>

                                <p>Гарантия сроков доставки</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="contact-form" id="contact-form">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <h2 class="block-title">Остались вопросы или хотите оформить заказ?</h2>
                        <p>Мы с радостью вам поможем!</p>
                        <form action="send.php">
                            <div class="inputs-wrapper">
                                <div class="inputs">
                                    <div class="form-group">
                                        <label for="name-input">имя</label>
                                        <input name="name" type="text" class="form-control" id="name-input" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone-input">телефон</label>
                                        <input name="phone" type="tel" class="form-control" id="phone-input" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email-input">email</label>
                                        <input name="email" type="email" class="form-control" id="email-input" required>
                                    </div>
                                </div>

                                <div class="form-group textarea-group">
                                    <label for="question-input">ваш вопрос</label>
                                    <textarea name="question" id="question-input" required></textarea>
                                </div>
                            </div>

                            <button type="submit" class="button">отправить</button>
                        </form>
                    </div>
                </div>
            </div>
            <img src="img/wingo-boxes.png" alt="" class="pic">
        </section>

        <section class="contacts">
            <div class="content">
                <div class="contacts-wrapper">
                    <img src="img/logo.svg" alt="" class="logo-contacts">
                    <div class="all-info">
                        <div class="place-time">
                            <p>03110 г. Киев, <br> ул. Пироговского 19, корп.4</p>
                            <p>Пн-Пт.: 9:00-18:00, <br> Сб, Вс — выходной</p>
                        </div>
                        <div class="line"></div>
                        <div class="phons-mail">
                            <div class="phons">
                                <a href="tel:0445947333">+38(044) 594-73-33</a>
                                <a href="tel:380672493154">+38(067) 249-31-54</a>
                            </div>
                            <div class="mail">
                                <a href="skype:company.wingo?userinfo">Skype: company.wingo</a>
                                <a href="mailto:info@wingo.com.ua">Email: info@wingo.com.ua</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="map" id="map"></div>
            </div>
        </section>
        
        <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <p>Wingo 2009-2018</p>
                        <p>Copyright ©</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <?php include ('components/popup.php');?>


    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9edarAyv4pFdfZ7K3bq4-_jGf_wvakCo"></script> <!--&callback=initMap -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/wow.min.js"></script>
    <script src="js/likely.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.viewportchecker.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/js.js"></script>
    <script src="js/map.js"></script>
  </body>
</html>
