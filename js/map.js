function initMap() {
    var location = {
        lat: 50.4190565,
        lng: 30.47825754
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: location,
        scrollwheel: false,
        scrollwheel: false,
        mapTypeControl: false,
        zoomControl: false
    });

    var marker = new google.maps.Marker({
        position: location,
        map: map
    });

    $.getJSON("map-style_dark.json", function(data) {
        map.setOptions({styles: data});
    });
}

initMap();
