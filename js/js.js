$(function() {

    // ANIMATE INIT

    new WOW().init();

    // MAP INIT


    // MOB-MENU

    //$("#mob-menu").on("click", function(){
    //    $("#nav-menu").slideToggle();
    //    $(this).toggleClass("active-mobile-menu");
    //    $('#nav').toggleClass('menu-in');
    //    $("#nav-menu a").on("click", function () {
    //        $("#nav-menu").slideToggle();
    //        $("#mob-menu").removeClass("active-mobile-menu");
    //    });
    //});

    // SLIDER INIT

    $('#header-slider').slick({
        autoplay: true,
        arrows: false,
        dots: true,
        slidesToShow: 1,
        centerMode: false,
        draggable: false,
        infinite: true,
        pauseOnHover: true,
        swipe: false,
        touchMove: false,
        fade:false,
        autoplaySpeed: 5000,
        adaptiveHeight: true
    });

    // CASTOME SLIDER ARROWS

    $('#rev-slider').slick({
        autoplay: false,
        autoplaySpeed: 5000,
        slidesToShow: 1,
        centerPadding: '25%',
        slidesToScroll: 1,
        centerMode: true,
        arrows: false,
        fade:false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    centerPadding: '15%'
                }
            },
            {
                breakpoint: 575,
                settings: {
                    centerPadding: '0%',
                    adaptiveHeight: true
                }
            }
        ]
    });

    $('.reviews .prev').click(function(e){
        e.preventDefault();
        $('#rev-slider').slick('slickPrev');
    });

    $('.reviews .next').click(function(e){
        e.preventDefault();
        $('#rev-slider').slick('slickNext');
    });

    $('#rev-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){

        $('.slick-slide .slide-number').removeClass('active-number');
        $('.slick-center .slide-number').addClass('active-number');
    });

    $('#rev-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){

        $('.slick-slide .slide-number').removeClass('active-number');

    });

    //PARALLAX

    $('#animate-map').viewportChecker({
        classToAdd: 'animation-map-active', // Class to add to the elements when they are visible,
        removeClassAfterAnimation: false
    });

    $('.delivery-methods .method').viewportChecker({
        classToAdd: 'active-animation-method', // Class to add to the elements when they are visible,
        removeClassAfterAnimation: false
    });

    //

    $('input, textarea').on('focus', function () {
        $(this).closest('.form-group').find('label').addClass('active-label');
    });

    $('input, textarea').on('blur', function() {
        var $this = $(this);
        if ($this.val() == '') {
            $this.closest('.form-group').find('label').removeClass('active-label');
        }
    });

    $('.form-calculator select').on('focus', function () {
        $(this).closest('.form-group').find('label').addClass('active-label');
    });

    $('.form-calculator select').on('blur', function () {
        var $this = $(this);
        if ($this.val() == '') {
            $this.closest('.form-group').find('label').removeClass('active-label');
        }
    });

    //

    $('#mob-menu').on('click', function (e) {
       e.preventDefault();

       $(this).toggleClass('active-menu');
       $('.mob-contacts').slideToggle(200);
    });

    $('#mob-menu2').on('click', function (e) {
        e.preventDefault();

        $(this).toggleClass('active-menu');
        $('.scroll-menu .mob-contacts').slideToggle(200);
    });

    //


    $(".cantry-list").mCustomScrollbar({
        theme:"minimal-dark"
    });


    // SHOW SCROLL TOP BUTTON.

    $(document).scroll(function() {
        var y = $(this).scrollTop();

        var scrollOn = $('.word-map').offset().top;

       if (y > scrollOn) {
            $('.scroll-menu').slideDown(500);
        } else {
           $('.scroll-menu').slideUp(500);
        }
    });

    //

    $('#alphabet a').on('click', function () {
       $('#alphabet a').removeClass('active-litera');

       $(this).addClass('active-litera');
    });

    // SCROLL TOP.

    //$(document).on('click', '.up-btn', function() {
    //    $('html, body').animate({
    //        scrollTop: 0
    //    }, 300);
    //});

    //SCROLL MENU

    $(document).on('click', '.scroll-to', function (e) {
        e.preventDefault();

        var href = $(this).attr('href');

        $('html, body').animate({
            scrollTop: $(href).offset().top
        }, 1000)
    });

    // POPUPS

    //$(document).on('click', '.open-popup', function (e) {
    //    e.preventDefault();
//
    //    var popup = $(this).data('popup');
//
    //    $(popup).fadeIn();
    //});
//
    //$(document).on('click', '.close-popup', function (e) {
    //    e.preventDefault();
//
    //    $('.popup').fadeOut();
    //});

    // PRELOADER

    setTimeout( function () {
        $('#loader').fadeOut(400);
    }, 500);

    //$(window).on('load', function () {
    //    var $preloader = $('#preloader');
//
    //    $preloader.delay(500).fadeOut('slow');
    //});

    // CASTOME SLIDER ARROWS

   //$('.mein-slider').slick({
   //    autoplay: false,
   //    autoplaySpeed: 5000,
   //    slidesToShow: 1,
   //    slidesToScroll: 1,
   //    arrows: false,
   //    fade: true

   //});

   //$('.main-page .arrow-left').click(function(){
   //    $('.mein-slider').slick('slickPrev');
   //});

   //$('.main-page .arrow-right').click(function(){
   //    $('.mein-slider').slick('slickNext');
   //});

    // FORM LABEL FOCUS UP

    //$('.form-input').on('focus', function() {
    //    $(this).closest('.input-fild').find('label').addClass('active');
    //});
    //$('.form-input').on('blur', function() {
    //    var $this = $(this);
    //    if ($this.val() == '') {
    //        $this.closest('.input-fild').find('label').removeClass('active');
    //    }
    //});

    // DTA VALUE REPLACE

    //$('.open-form').on('click', function (e) {
    //    e.preventDefault();
    //    var type = $(this).data('type');
    //    $('#type-form').find('input[name=type]').val(type);
    //});

    // PHONE MASK

    //$("input[type=tel]").mask("+38(999) 999-99-99");


    // Выезжяющее меню справа
    //$('#open-menu').on('click', function (e) {
    //    e.preventDefault();
    //    $('#menu-wrap').toggleClass('menu-slider');
    //    $('#menu-wrap').toggleClass('menu-slider-out');
    //    $('.menu-slider-out').css({'opacity' : '1'});
//
    //});
});

// PRELOADER

$(window).on('load', function () {
    $('.slick-center .slide-number').addClass('active-number');
});
