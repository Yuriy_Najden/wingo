<div class="modal fade cantrys-modal" id="cantryModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <svg viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M33.349 1.95312L1.95361 33.3485" stroke="white" stroke-width="3"/>
                        <path d="M1.95377 1.95312L33.3491 33.3485" stroke="white" stroke-width="3"/>
                    </svg>
                </button>
            </div>
            <div class="modal-body">
                <div class="alphabet" id="alphabet">
                    <a href="#litera-1" class="active-litera">а</a>
                    <a href="#litera-2">б</a>
                    <a href="#litera-3">в</a>
                    <a href="#litera-4">г</a>
                    <a href="#litera-5">д</a>
                    <a href="#litera-6">е</a>
                    <a href="#" disabled="">ж</a>
                    <a href="#litera-7">з</a>
                    <a href="#litera-8">и</a>
                    <a href="#litera-9">к</a>
                    <a href="#litera-10">л</a>
                    <a href="#litera-11">м</a>
                    <a href="#litera-12">н</a>
                    <a href="#litera-13">о</a>
                    <a href="#litera-14">п</a>
                    <a href="#litera-15">р</a>
                    <a href="#litera-16">с</a>
                    <a href="#litera-17">т</a>
                    <a href="#litera-18">у</a>
                    <a href="#litera-19">ф</a>
                    <a href="#litera-20">х</a>
                    <a href="#litera-21">ц</a>
                    <a href="#litera-22">ч</a>
                    <a href="#litera-23">ш</a>
                    <a href="#litera-24">э</a>
                    <a href="#litera-25">ю</a>
                    <a href="#litera-26">я</a>
                </div>
                <div class="cantry-list">
                    <a href="#" class="litera" id="litera-1">
                        <div class="name">
                            <p>a</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Австралия</p>
                            <p>Австрия</p>
                            <p>Азербайджан</p>
                            <p>Албания</p>
                            <p>Алжир</p>
                            <p>Американское Самоа</p>
                            <p>Ангилья</p>
                            <p>Ангола</p>
                            <p>Андорра</p>
                            <p>Антигуа</p>
                            <p>Аргентина</p>
                            <p>Армения</p>
                            <p>Аруба</p>
                            <p>Афганистан</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-2">
                        <div class="name">
                            <p>б</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Багамские Острова</p>
                            <p>Бангладеш</p>
                            <p>Барбадос</p>
                            <p>Бахрейн</p>
                            <p>Белиз</p>
                            <p>Белоруссия</p>
                            <p>Бельгия</p>
                            <p>Бенин</p>
                            <p>Бермудские Острова</p>
                            <p>Болгария</p>
                            <p>Боливия</p>
                            <p>Бонер</p>
                            <p>Босния и Герцеговина</p>
                            <p>Ботсвана</p>
                            <p>Бразилия</p>
                            <p>Бруней</p>
                            <p>Буркина-Фасо</p>
                            <p>Бурунди</p>
                            <p>Бутан</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-3">
                        <div class="name">
                            <p>в</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Вануату</p>
                            <p>Великобритания</p>
                            <p>Венгрия</p>
                            <p>Венесуэла</p>
                            <p>Виргинские острова (Британия)</p>
                            <p>Виргинские острова (США)</p>
                            <p>Восточный Тимор</p>
                            <p>Вьетнам</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-4">
                        <div class="name">
                            <p>г</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Габон</p>
                            <p>Гайана</p>
                            <p>Гамбия</p>
                            <p>Гана</p>
                            <p>Гваделупа</p>
                            <p>Гватемала</p>
                            <p>Гвинейская Республика</p>
                            <p>Гвинея-Бисау</p>
                            <p>Германия</p>
                            <p>Гернси</p>
                            <p>Гибралтар</p>
                            <p>Гондурас</p>
                            <p>Гонконг</p>
                            <p>Гренада</p>
                            <p>Гренландия</p>
                            <p>Греция</p>
                            <p>Грузия</p>
                            <p>Гуам</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-5">
                        <div class="name">
                            <p>д</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Дания</p>
                            <p>Демократическая Республика Конго</p>
                            <p>Джерси</p>
                            <p>Джибути</p>
                            <p>Доминика</p>
                            <p>Доминиканская Республика</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-6">
                        <div class="name">
                            <p>е</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Египет</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-7">
                        <div class="name">
                            <p>з</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Замбия</p>
                            <p>Зимбабве</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-8">
                        <div class="name">
                            <p>и</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Израиль</p>
                            <p>Йемен</p>
                            <p>Индия</p>
                            <p>Индонезия</p>
                            <p>Иордания</p>
                            <p>Ирак</p>
                            <p>Иран</p>
                            <p>Ирландия</p>
                            <p>Исландия</p>
                            <p>Испания</p>
                            <p>Италия</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-9">
                        <div class="name">
                            <p>к</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Казахстан</p>
                            <p>Каймановы острова</p>
                            <p>Камбоджа</p>
                            <p>Камерун</p>
                            <p>Канада</p>
                            <p>Канарские острова</p>
                            <p>Катар</p>
                            <p>Кейп-Верде</p>
                            <p>Кения</p>
                            <p>Кипр</p>
                            <p>Киргизия</p>
                            <p>Кирибати</p>
                            <p>Китай</p>
                            <p>Колумбия</p>
                            <p>Коморские острова</p>
                            <p>Конго</p>
                            <p>Косово</p>
                            <p>Коста-Рика</p>
                            <p>Кот-д’Ивуар</p>
                            <p>Куба</p>
                            <p>Кувейт</p>
                            <p>Кюрасао</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-10">
                        <div class="name">
                            <p>л</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Лаос</p>
                            <p>Латвия</p>
                            <p>Лесото</p>
                            <p>Либерия</p>
                            <p>Ливан</p>
                            <p>Ливия</p>
                            <p>Литва</p>
                            <p>Лихтвенштейн</p>
                            <p>Люксембург</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-11">
                        <div class="name">
                            <p>м</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Мавритания</p>
                            <p>Мавритиус</p>
                            <p>Мадагаскар</p>
                            <p>Майотта</p>
                            <p>Макао</p>
                            <p>Македония</p>
                            <p>Малави</p>
                            <p>Малайзия</p>
                            <p>Мали</p>
                            <p>Мальдивы</p>
                            <p>Мальта</p>
                            <p>Марокко</p>
                            <p>Мартиника</p>
                            <p>Маршалловы Острова</p>
                            <p>Мексика</p>
                            <p>Микронезия</p>
                            <p>Мозамбик</p>
                            <p>Молдавия</p>
                            <p>Монако</p>
                            <p>Монголия</p>
                            <p>Монсеррат</p>
                            <p>Монтенегро</p>
                            <p>Мьянма (Бирма)</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-12">
                        <div class="name">
                            <p>н</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Норвегия</p>
                            <p>Новая Каледония</p>
                            <p>Новая Зеландия</p>
                            <p>Ниу острова</p>
                            <p>Никарагуа</p>
                            <p>Нидерланды</p>
                            <p>Нидерландские Антильские острова</p>
                            <p>Нигерия</p>
                            <p>Нигер</p>
                            <p>Непал</p>
                            <p>Невис</p>
                            <p>Науру</p>
                            <p>Намибия</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-13">
                        <div class="name">
                            <p>о</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Острова Кука</p>
                            <p>Объединённые Арабские Эмираты</p>
                            <p>Остров Святой Елены</p>
                            <p>Оман</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-14">
                        <div class="name">
                            <p>п</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Пакистан</p>
                            <p>Палау</p>
                            <p>Панама</p>
                            <p>Папуа Новая Гвинея</p>
                            <p>Парагвай</p>
                            <p>Перу</p>
                            <p>Польша</p>
                            <p>Португалия</p>
                            <p>Пуэрто-Рико</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-15">
                        <div class="name">
                            <p>р</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Республика Гаити</p>
                            <p>Румыния</p>
                            <p>Руанда</p>
                            <p>Реюньон</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-16">
                        <div class="name">
                            <p>с</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Сайпан</p>
                            <p>Сальвадор</p>
                            <p>Самоа</p>
                            <p>Сан-Марино</p>
                            <p>Санта-Лючия</p>
                            <p>Сан-Томе и Принсипи</p>
                            <p>Саудовская Аравия</p>
                            <p>Свазиленд</p>
                            <p>Северная Корея</p>
                            <p>Сейшельские Острова</p>
                            <p>Сен-Бартелеми</p>
                            <p>Сенегал</p>
                            <p>Сен-Мартен</p>
                            <p>Сент-Винсент и Гренадины</p>
                            <p>Сент-Китс</p>
                            <p>Сербия</p>
                            <p>Сингапур</p>
                            <p>Синт-Эстатиус</p>
                            <p>Сирия</p>
                            <p>Словакия</p>
                            <p>Словения</p>
                            <p>Соломоновы Острова</p>
                            <p>Сомали</p>
                            <p>Сомалиленд</p>
                            <p>Судан</p>
                            <p>Суринам</p>
                            <p>США</p>
                            <p>Сьерра-Леоне</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-17">
                        <div class="name">
                            <p>т</p>
                            <hr>
                        </div>
                        <div class="litera-list colums">
                            <p>Таджикистан</p>
                            <p>Тайвань</p>
                            <p>Таиланд</p>
                            <p>Таити</p>
                            <p>Танзания</p>
                            <p>Теркс и Кайкос</p>
                            <p>Того</p>
                            <p>Тонга</p>
                            <p>Тринидад и Тобаго</p>
                            <p>Тувалу</p>
                            <p>Тунис</p>
                            <p>Турция</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-18">
                        <div class="name">
                            <p>у</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Уругвай</p>
                            <p>Украина</p>
                            <p>Узбекистан</p>
                            <p>Уганда</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-19">
                        <div class="name">
                            <p>ф</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Фарерские острова</p>
                            <p>Фиджи</p>
                            <p>Филиппины</p>
                            <p>Финляндия</p>
                            <p>Фолклендские острова</p>
                            <p>Франция</p>
                            <p>Французская Гвиана</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-20">
                        <div class="name">
                            <p>х</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Хорватия</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-21">
                        <div class="name">
                            <p>ц</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Центральноафриканская Республика</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-22">
                        <div class="name">
                            <p>ч</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Чад</p>
                            <p>Чехия</p>
                            <p>Чили</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-23">
                        <div class="name">
                            <p>ш</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Швейцария</p>
                            <p>Шри-Ланка</p>
                            <p>Швеция</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-24">
                        <div class="name">
                            <p>э</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Эквадор</p>
                            <p>Экваториальная Гвинея</p>
                            <p>Эритрея</p>
                            <p>Эстония</p>
                            <p>Эфиопия</p>
                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-25">
                        <div class="name">
                            <p>ю</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Южная Корея</p>
                            <p>Южно-Африканская Республика</p>

                        </div>
                    </a>
                    <a href="#" class="litera" id="litera-26">
                        <div class="name">
                            <p>я</p>
                            <hr>
                        </div>
                        <div class="litera-list">
                            <p>Ямайка</p>
                            <p>Япония</p>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>