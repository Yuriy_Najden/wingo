<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="" />
    <meta property="og:description" content="Доставка грузов «под ключ»" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="/img/og.jpg" />

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#001066">

    <meta name="theme-color" content="#001066">

    <title>Wingo</title>

    <link rel="stylesheet" href="css/likely.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css"/>

</head>
<body>
<div class="wrapper calc-page">
    <header>
        <div class="top-line">
            <div class="container-fluid">
                <div class="row">
                    <div class="content col-12">
                        <div class="logo">
                            <img src="img/logo.svg" alt="">
                        </div>
                        <a class="mob-menu" id="mob-menu">
                            <div class="top"></div>
                            <div class="center"></div>
                            <div class="bottom"></div>
                        </a>
                        <div class="mob-contacts">
                            <img src="img/scroll-logo.svg" alt="" class="menu-logo">
                            <div class="center-conteiner">
                                <h3>контакты</h3>
                                <div class="phone-wrapper">
                                    <a href="tel:380672493154">+38(067) 249-31-54</a>
                                    <a href="tel:380445947333">+38(044) 594-73-33</a>
                                </div>
                                <div class="soc-wrapper">
                                    <a href="#">
                                        <svg width="11" height="22" viewBox="0 0 11 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0.322864 11.6827H2.51715V21.0775C2.51715 21.263 2.66165 21.4133 2.84001 21.4133H6.56045C6.73881 21.4133 6.88331 21.263 6.88331 21.0775V11.727H9.40582C9.56983 11.727 9.7078 11.599 9.72653 11.4295L10.1096 7.97083C10.1202 7.87568 10.0912 7.78038 10.03 7.70901C9.9687 7.63753 9.881 7.59663 9.78897 7.59663H6.88343V5.42858C6.88343 4.77499 7.22182 4.4436 7.88924 4.4436C7.98437 4.4436 9.78897 4.4436 9.78897 4.4436C9.96733 4.4436 10.1118 4.29324 10.1118 4.10778V0.933047C10.1118 0.747551 9.96733 0.597268 9.78897 0.597268H7.17087C7.15238 0.596333 7.11137 0.594788 7.05096 0.594788C6.59665 0.594788 5.01768 0.687535 3.77037 1.8809C2.38836 3.20336 2.58049 4.78674 2.62639 5.06129V7.59659H0.322864C0.144503 7.59659 0 7.74687 0 7.93237V11.3468C0 11.5323 0.144503 11.6827 0.322864 11.6827Z" fill="white"/>
                                        </svg>
                                    </a>
                                    <a href="#">
                                        <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.444 0H7.54852C3.93334 0 0.992188 2.94116 0.992188 6.55633V15.4518C0.992188 19.0669 3.93334 22.0081 7.54852 22.0081H16.444C20.0591 22.0081 23.0003 19.0669 23.0003 15.4518V6.55633C23.0003 2.94116 20.0591 0 16.444 0ZM20.7863 15.4518C20.7863 17.8499 18.8421 19.7941 16.444 19.7941H7.54852C5.15034 19.7941 3.2062 17.8499 3.2062 15.4518V6.55633C3.2062 4.15812 5.15034 2.21401 7.54852 2.21401H16.444C18.8421 2.21401 20.7863 4.15812 20.7863 6.55633V15.4518Z" fill="white"/>
                                            <path d="M11.9957 5.31232C8.85714 5.31232 6.30371 7.8661 6.30371 11.0051C6.30371 14.144 8.85714 16.6979 11.9957 16.6979C15.1343 16.6979 17.6877 14.1441 17.6877 11.0051C17.6877 7.86606 15.1343 5.31232 11.9957 5.31232ZM11.9957 14.4836C10.0749 14.4836 8.5177 12.9262 8.5177 11.0051C8.5177 9.08397 10.0749 7.52661 11.9957 7.52661C13.9166 7.52661 15.4738 9.08397 15.4738 11.0051C15.4738 12.9262 13.9166 14.4836 11.9957 14.4836Z" fill="white"/>
                                            <path d="M17.6994 6.71915C18.4527 6.71915 19.0633 6.10849 19.0633 5.35521C19.0633 4.60193 18.4527 3.99127 17.6994 3.99127C16.9461 3.99127 16.3354 4.60193 16.3354 5.35521C16.3354 6.10849 16.9461 6.71915 17.6994 6.71915Z" fill="white"/>
                                        </svg>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="phone-soc">
                            <div class="phone-wrapper">
                                <a href="tel:380672493154">+38(067) 249-31-54</a>
                                <a href="tel:380445947333">+38(044) 594-73-33</a>
                            </div>
                            <div class="line"></div>
                            <div class="soc-wrapper">
                                <a href="#">
                                    <svg width="11" height="22" viewBox="0 0 11 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0.322864 11.6827H2.51715V21.0775C2.51715 21.263 2.66165 21.4133 2.84001 21.4133H6.56045C6.73881 21.4133 6.88331 21.263 6.88331 21.0775V11.727H9.40582C9.56983 11.727 9.7078 11.599 9.72653 11.4295L10.1096 7.97083C10.1202 7.87568 10.0912 7.78038 10.03 7.70901C9.9687 7.63753 9.881 7.59663 9.78897 7.59663H6.88343V5.42858C6.88343 4.77499 7.22182 4.4436 7.88924 4.4436C7.98437 4.4436 9.78897 4.4436 9.78897 4.4436C9.96733 4.4436 10.1118 4.29324 10.1118 4.10778V0.933047C10.1118 0.747551 9.96733 0.597268 9.78897 0.597268H7.17087C7.15238 0.596333 7.11137 0.594788 7.05096 0.594788C6.59665 0.594788 5.01768 0.687535 3.77037 1.8809C2.38836 3.20336 2.58049 4.78674 2.62639 5.06129V7.59659H0.322864C0.144503 7.59659 0 7.74687 0 7.93237V11.3468C0 11.5323 0.144503 11.6827 0.322864 11.6827Z" fill="white"/>
                                    </svg>
                                </a>
                                <a href="#">
                                    <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M16.444 0H7.54852C3.93334 0 0.992188 2.94116 0.992188 6.55633V15.4518C0.992188 19.0669 3.93334 22.0081 7.54852 22.0081H16.444C20.0591 22.0081 23.0003 19.0669 23.0003 15.4518V6.55633C23.0003 2.94116 20.0591 0 16.444 0ZM20.7863 15.4518C20.7863 17.8499 18.8421 19.7941 16.444 19.7941H7.54852C5.15034 19.7941 3.2062 17.8499 3.2062 15.4518V6.55633C3.2062 4.15812 5.15034 2.21401 7.54852 2.21401H16.444C18.8421 2.21401 20.7863 4.15812 20.7863 6.55633V15.4518Z" fill="white"/>
                                        <path d="M11.9957 5.31232C8.85714 5.31232 6.30371 7.8661 6.30371 11.0051C6.30371 14.144 8.85714 16.6979 11.9957 16.6979C15.1343 16.6979 17.6877 14.1441 17.6877 11.0051C17.6877 7.86606 15.1343 5.31232 11.9957 5.31232ZM11.9957 14.4836C10.0749 14.4836 8.5177 12.9262 8.5177 11.0051C8.5177 9.08397 10.0749 7.52661 11.9957 7.52661C13.9166 7.52661 15.4738 9.08397 15.4738 11.0051C15.4738 12.9262 13.9166 14.4836 11.9957 14.4836Z" fill="white"/>
                                        <path d="M17.6994 6.71915C18.4527 6.71915 19.0633 6.10849 19.0633 5.35521C19.0633 4.60193 18.4527 3.99127 17.6994 3.99127C16.9461 3.99127 16.3354 4.60193 16.3354 5.35521C16.3354 6.10849 16.9461 6.71915 17.6994 6.71915Z" fill="white"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="form-calculator">
        <div class="container-fluid">
            <div class="row">
                <div class="content col-12">
                    <h2 class="block-title">Рассчет стоимости доставки</h2>
                    <form action="send.php">
                        <div class="form-group">
                            <label for="name-input2">имя</label>
                            <input name="name" type="text" class="form-control" id="name-input2" required>
                        </div>
                        <div class="form-group">
                            <label for="phone-input2">телефон</label>
                            <input name="phone" type="tel" class="form-control" id="phone-input2" required>
                        </div>
                        <div class="form-group">
                            <label for="email-input2">email</label>
                            <input name="email" type="email" class="form-control" id="email-input2" required>
                        </div>
                        <div class="form-group">
                            <label for="whence-input">откуда</label>
                            <select name="whence" id="whence-input" required>
                                <option selected value=""></option>
                                <option value="Австралия">Австралия</option>
                                <option value="Австрия">Австрия</option>
                                <option value="Азербайджан">Азербайджан</option>
                                <option value="Албания">Албания</option>
                                <option value="Алжир">Алжир</option>
                                <option value="Американское Самоа">Американское Самоа</option>
                                <option value="Ангилья">Ангилья</option>
                                <option value="Ангола">Ангола</option>
                                <option value="Андорра">Андорра</option>
                                <option value="Антигуа">Антигуа</option>
                                <option value="Аргентина">Аргентина</option>
                                <option value="Армения">Армения</option>
                                <option value="Аруба">Аруба</option>
                                <option value="Афганистан">Афганистан</option>
                                <option value="Багамские Острова">Багамские Острова</option>
                                <option value="Бангладеш">Бангладеш</option>
                                <option value="Барбадос">Барбадос</option>
                                <option value="Бахрейн">Бахрейн</option>
                                <option value="Белиз">Белиз</option>
                                <option value="Белоруссия">Белоруссия</option>
                                <option value="Бельгия">Бельгия</option>
                                <option value="Бенин">Бенин</option>
                                <option value="Бермудские Острова">Бермудские Острова</option>
                                <option value="Болгария">Болгария</option>
                                <option value="Боливия">Боливия</option>
                                <option value="Бонер">Бонер</option>
                                <option value="Босния и Герцеговина">Босния и Герцеговина</option>
                                <option value="Ботсвана">Ботсвана</option>
                                <option value="Бразилия">Бразилия</option>
                                <option value="Бруней">Бруней</option>
                                <option value="Буркина-Фасо">Буркина-Фасо</option>
                                <option value="Вануату">Вануату</option>
                                <option value="Великобритания">Великобритания</option>
                                <option value="Венгрия">Венгрия</option>
                                <option value="Венесуэла">Венесуэла</option>
                                <option value="Виргинские острова (Британия)">Виргинские острова (Британия)</option>
                                <option value="Виргинские острова (США)">Виргинские острова (США)</option>
                                <option value="Восточный Тимор">Восточный Тимор</option>
                                <option value="Вьетнам">Вьетнам</option>
                                <option value="Габон">Габон</option>
                                <option value="Гайана">Гайана</option>
                                <option value="Гамбия">Гамбия</option>
                                <option value="Гана">Гана</option>
                                <option value="Гваделупа">Гваделупа</option>
                                <option value="Гватемала">Гватемала</option>
                                <option value="Гвинейская Республика">Гвинейская Республика</option>
                                <option value="Гвинея-Бисау">Гвинея-Бисау</option>
                                <option value="Германия">Германия</option>
                                <option value="Гернси">Гернси</option>
                                <option value="Гибралтар">Гибралтар</option>
                                <option value="Гондурас">Гондурас</option>
                                <option value="Гонконг">Гонконг</option>
                                <option value="Гренада">Гренада</option>
                                <option value="Гренландия">Гренландия</option>
                                <option value="Греция">Греция</option>
                                <option value="Грузия">Грузия</option>
                                <option value="Дания">Дания</option>
                                <option value="Демократическая Республика Конго">Демократическая Республика Конго</option>
                                <option value="Джерси">Джерси</option>
                                <option value="Джибути">Джибути</option>
                                <option value="Доминика">Доминика</option>
                                <option value="Доминиканская Республика">Доминиканская Республика</option>
                                <option value="Египет">Египет</option>
                                <option value="Замбия">Замбия</option>
                                <option value="Зимбабве">Зимбабве</option>
                                <option value="Израиль">Израиль</option>
                                <option value="Йемен">Йемен</option>
                                <option value="Индия">Индия</option>
                                <option value="Индонези">Индонези</option>
                                <option value="Иордания">Иордания</option>
                                <option value="Ирак">Ирак</option>
                                <option value="Иран">Иран</option>
                                <option value="Ирландия">Ирландия</option>
                                <option value="Исландия">Исландия</option>
                                <option value="Испания">Испания</option>
                                <option value="Италия">Италия</option>
                                <option value="Казахстан">Казахстан</option>
                                <option value="Каймановы острова">Каймановы острова</option>
                                <option value="Камбоджа">Камбоджа</option>
                                <option value="Камерун">Камерун</option>
                                <option value="Канада">Канада</option>
                                <option value="Канарские острова">Канарские острова</option>
                                <option value="Катар">Катар</option>
                                <option value="Кейп-Верде">Кейп-Верде</option>
                                <option value="Кения">Кения</option>
                                <option value="Кипр">Кипр</option>
                                <option value="Киргизия">Киргизия</option>
                                <option value="Кирибати">Кирибати</option>
                                <option value="Китай">Китай</option>
                                <option value="Колумбия">Колумбия</option>
                                <option value="Коморские острова">Коморские острова</option>
                                <option value="Конго">Конго</option>
                                <option value="Косово">Косово</option>
                                <option value="Коста-Рика">Коста-Рика</option>
                                <option value="Кот-д’Ивуар">Кот-д’Ивуар</option>
                                <option value="Куба">Куба</option>
                                <option value="Кувейт">Кувейт</option>
                                <option value="Кюрасао">Кюрасао</option>
                                <option value="Лаос">Лаос</option>
                                <option value="Латвия">Латвия</option>
                                <option value="Лесото">Лесото</option>
                                <option value="Либерия">Либерия</option>
                                <option value="Ливан">Ливан</option>
                                <option value="Ливия">Ливия</option>
                                <option value="Литва">Литва</option>
                                <option value="Лихтвенштейн">Лихтвенштейн</option>
                                <option value="Люксембург">Люксембург</option>
                                <option value="Мавритания">Мавритания</option>
                                <option value="Мавритиус">Мавритиус</option>
                                <option value="Мадагаскар">Мадагаскар</option>
                                <option value="Майотта">Майотта</option>
                                <option value="Макао">Макао</option>
                                <option value="Македония">Македония</option>
                                <option value="Малави">Малави</option>
                                <option value="Малайзия">Малайзия</option>
                                <option value="Мали">Мали</option>
                                <option value="Мальдивы">Мальдивы</option>
                                <option value="Мальта">Мальта</option>
                                <option value="Марокко">Марокко</option>
                                <option value="Мартиника">Мартиника</option>
                                <option value="Маршалловы Острова">Маршалловы Острова</option>
                                <option value="Мексика">Мексика</option>
                                <option value="Микронезия">Микронезия</option>
                                <option value="Мозамбик">Мозамбик</option>
                                <option value="Молдавия">Молдавия</option>
                                <option value="Монако">Монако</option>
                                <option value="Монголия">Монголия</option>
                                <option value="Монсеррат">Монсеррат</option>
                                <option value="Монтенегро">Монтенегро</option>
                                <option value="Мьянма (Бирма)">Мьянма (Бирма)</option>
                                <option value="Норвегия">Норвегия</option>
                                <option value="Новая Каледония">Новая Каледония</option>
                                <option value="Новая Зеландия">Новая Зеландия</option>
                                <option value="Ниу острова">Ниу острова</option>
                                <option value="Никарагуа">Никарагуа</option>
                                <option value="Нидерланды">Нидерланды</option>
                                <option value="Нидерландские Антильские острова">Нидерландские Антильские острова</option>
                                <option value="Нигерия">Нигерия</option>
                                <option value="Нигер">Нигер</option>
                                <option value="Непал">Непал</option>
                                <option value="Невис">Невис</option>
                                <option value="Острова Кука">Острова Кука</option>
                                <option value="Объединённые Арабские Эмираты">Объединённые Арабские Эмираты</option>
                                <option value="Остров Святой Елены">Остров Святой Елены</option>
                                <option value="Оман">Оман</option>
                                <option value="Пакистан">Пакистан</option>
                                <option value="Палау">Палау</option>
                                <option value="Панама">Панама</option>
                                <option value="Папуа Новая Гвинея">Папуа Новая Гвинея</option>
                                <option value="Парагвай">Парагвай</option>
                                <option value="Перу">Перу</option>
                                <option value="Польша">Польша</option>
                                <option value="Португалия">Португалия</option>
                                <option value="Пуэрто-Рико">Пуэрто-Рико</option>
                                <option value="Республика Гаити">Республика Гаити</option>
                                <option value="Румыния">Румыния</option>
                                <option value="Руанда">Руанда</option>
                                <option value="Реюньон">Реюньон</option>
                                <option value="Сайпан">Сайпан</option>
                                <option value="Сальвадор">Сальвадор</option>
                                <option value="Самоа">Самоа</option>
                                <option value="Сан-Марино">Сан-Марино</option>
                                <option value="Санта-Лючия">Санта-Лючия</option>
                                <option value="Сан-Томе и Принсипи">Сан-Томе и Принсипи</option>
                                <option value="Саудовская Аравия">Саудовская Аравия</option>
                                <option value="Свазиленд">Свазиленд</option>
                                <option value="Северная Корея">Северная Корея</option>
                                <option value="Сейшельские Острова">Сейшельские Острова</option>
                                <option value="Сен-Бартелеми">Сен-Бартелеми</option>
                                <option value="Сенегал">Сенегал</option>
                                <option value="Сен-Мартен">Сен-Мартен</option>
                                <option value="Сент-Винсент и Гренадины">Сент-Винсент и Гренадины</option>
                                <option value="Сент-Китс">Сент-Китс</option>
                                <option value="Сербия">Сербия</option>
                                <option value="Сингапур">Сингапур</option>
                                <option value="Синт-Эстатиус">Синт-Эстатиус</option>
                                <option value="Сирия">Сирия</option>
                                <option value="Словакия">Словакия</option>
                                <option value="Словения">Словения</option>
                                <option value="Соломоновы Острова">Соломоновы Острова</option>
                                <option value="Сомали">Сомали</option>
                                <option value="Сомалиленд">Сомалиленд</option>
                                <option value="Судан">Судан</option>
                                <option value="Суринам">Суринам</option>
                                <option value="США">США</option>
                                <option value="Сьерра-Леоне">Сьерра-Леоне</option>
                                <option value="Таджикистан">Таджикистан</option>
                                <option value="Тайвань">Тайвань</option>
                                <option value="Таиланд">Таиланд</option>
                                <option value="Таити">Таити</option>
                                <option value="Танзания">Танзания</option>
                                <option value="Теркс и Кайкос">Теркс и Кайкос</option>
                                <option value="Того">Того</option>
                                <option value="Тонга">Тонга</option>
                                <option value="Тринидад и Тобаго">Тринидад и Тобаго</option>
                                <option value="Уругвай">Уругвай</option>
                                <option value="Украина">Украина</option>
                                <option value="Узбекистан">Узбекистан</option>
                                <option value="Уганда">Уганда</option>
                                <option value="Фарерские острова">Фарерские острова</option>
                                <option value="Фиджи">Фиджи</option>
                                <option value="Филиппины">Филиппины</option>
                                <option value="Финляндия">Финляндия</option>
                                <option value="Фолклендские острова">Фолклендские острова</option>
                                <option value="Франция">Франция</option>
                                <option value="Французская Гвиана">Французская Гвиана</option>
                                <option value="Хорватия">Хорватия</option>
                                <option value="Центральноафриканская Республика">Центральноафриканская Республика</option>
                                <option value="Чад">Чад</option>
                                <option value="Чехия">Чехия</option>
                                <option value="Чили">Чили</option>
                                <option value="Швейцария">Швейцария</option>
                                <option value="Шри-Ланка">Шри-Ланка</option>
                                <option value="Швеция">Швеция</option>
                                <option value="Эквадор">Эквадор</option>
                                <option value="Экваториальная Гвинея">Экваториальная Гвинея</option>
                                <option value="Эритрея">Эритрея</option>
                                <option value="Эстония">Эстония</option>
                                <option value="Эфиопия">Эфиопия</option>
                                <option value="Южная Корея">Южная Корея</option>
                                <option value="Южно-Африканская Республика">Южно-Африканская Республика</option>
                                <option value="Ямайка">Ямайка</option>
                                <option value="Япония">Япония</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="where-input">куда</label>
                            <select name="where" id="where-input" required>
                                <option selected value=""></option>
                                <option value="Австралия">Австралия</option>
                                <option value="Австрия">Австрия</option>
                                <option value="Азербайджан">Азербайджан</option>
                                <option value="Албания">Албания</option>
                                <option value="Алжир">Алжир</option>
                                <option value="Американское Самоа">Американское Самоа</option>
                                <option value="Ангилья">Ангилья</option>
                                <option value="Ангола">Ангола</option>
                                <option value="Андорра">Андорра</option>
                                <option value="Антигуа">Антигуа</option>
                                <option value="Аргентина">Аргентина</option>
                                <option value="Армения">Армения</option>
                                <option value="Аруба">Аруба</option>
                                <option value="Афганистан">Афганистан</option>
                                <option value="Багамские Острова">Багамские Острова</option>
                                <option value="Бангладеш">Бангладеш</option>
                                <option value="Барбадос">Барбадос</option>
                                <option value="Бахрейн">Бахрейн</option>
                                <option value="Белиз">Белиз</option>
                                <option value="Белоруссия">Белоруссия</option>
                                <option value="Бельгия">Бельгия</option>
                                <option value="Бенин">Бенин</option>
                                <option value="Бермудские Острова">Бермудские Острова</option>
                                <option value="Болгария">Болгария</option>
                                <option value="Боливия">Боливия</option>
                                <option value="Бонер">Бонер</option>
                                <option value="Босния и Герцеговина">Босния и Герцеговина</option>
                                <option value="Ботсвана">Ботсвана</option>
                                <option value="Бразилия">Бразилия</option>
                                <option value="Бруней">Бруней</option>
                                <option value="Буркина-Фасо">Буркина-Фасо</option>
                                <option value="Вануату">Вануату</option>
                                <option value="Великобритания">Великобритания</option>
                                <option value="Венгрия">Венгрия</option>
                                <option value="Венесуэла">Венесуэла</option>
                                <option value="Виргинские острова (Британия)">Виргинские острова (Британия)</option>
                                <option value="Виргинские острова (США)">Виргинские острова (США)</option>
                                <option value="Восточный Тимор">Восточный Тимор</option>
                                <option value="Вьетнам">Вьетнам</option>
                                <option value="Габон">Габон</option>
                                <option value="Гайана">Гайана</option>
                                <option value="Гамбия">Гамбия</option>
                                <option value="Гана">Гана</option>
                                <option value="Гваделупа">Гваделупа</option>
                                <option value="Гватемала">Гватемала</option>
                                <option value="Гвинейская Республика">Гвинейская Республика</option>
                                <option value="Гвинея-Бисау">Гвинея-Бисау</option>
                                <option value="Германия">Германия</option>
                                <option value="Гернси">Гернси</option>
                                <option value="Гибралтар">Гибралтар</option>
                                <option value="Гондурас">Гондурас</option>
                                <option value="Гонконг">Гонконг</option>
                                <option value="Гренада">Гренада</option>
                                <option value="Гренландия">Гренландия</option>
                                <option value="Греция">Греция</option>
                                <option value="Грузия">Грузия</option>
                                <option value="Дания">Дания</option>
                                <option value="Демократическая Республика Конго">Демократическая Республика Конго</option>
                                <option value="Джерси">Джерси</option>
                                <option value="Джибути">Джибути</option>
                                <option value="Доминика">Доминика</option>
                                <option value="Доминиканская Республика">Доминиканская Республика</option>
                                <option value="Египет">Египет</option>
                                <option value="Замбия">Замбия</option>
                                <option value="Зимбабве">Зимбабве</option>
                                <option value="Израиль">Израиль</option>
                                <option value="Йемен">Йемен</option>
                                <option value="Индия">Индия</option>
                                <option value="Индонези">Индонези</option>
                                <option value="Иордания">Иордания</option>
                                <option value="Ирак">Ирак</option>
                                <option value="Иран">Иран</option>
                                <option value="Ирландия">Ирландия</option>
                                <option value="Исландия">Исландия</option>
                                <option value="Испания">Испания</option>
                                <option value="Италия">Италия</option>
                                <option value="Казахстан">Казахстан</option>
                                <option value="Каймановы острова">Каймановы острова</option>
                                <option value="Камбоджа">Камбоджа</option>
                                <option value="Камерун">Камерун</option>
                                <option value="Канада">Канада</option>
                                <option value="Канарские острова">Канарские острова</option>
                                <option value="Катар">Катар</option>
                                <option value="Кейп-Верде">Кейп-Верде</option>
                                <option value="Кения">Кения</option>
                                <option value="Кипр">Кипр</option>
                                <option value="Киргизия">Киргизия</option>
                                <option value="Кирибати">Кирибати</option>
                                <option value="Китай">Китай</option>
                                <option value="Колумбия">Колумбия</option>
                                <option value="Коморские острова">Коморские острова</option>
                                <option value="Конго">Конго</option>
                                <option value="Косово">Косово</option>
                                <option value="Коста-Рика">Коста-Рика</option>
                                <option value="Кот-д’Ивуар">Кот-д’Ивуар</option>
                                <option value="Куба">Куба</option>
                                <option value="Кувейт">Кувейт</option>
                                <option value="Кюрасао">Кюрасао</option>
                                <option value="Лаос">Лаос</option>
                                <option value="Латвия">Латвия</option>
                                <option value="Лесото">Лесото</option>
                                <option value="Либерия">Либерия</option>
                                <option value="Ливан">Ливан</option>
                                <option value="Ливия">Ливия</option>
                                <option value="Литва">Литва</option>
                                <option value="Лихтвенштейн">Лихтвенштейн</option>
                                <option value="Люксембург">Люксембург</option>
                                <option value="Мавритания">Мавритания</option>
                                <option value="Мавритиус">Мавритиус</option>
                                <option value="Мадагаскар">Мадагаскар</option>
                                <option value="Майотта">Майотта</option>
                                <option value="Макао">Макао</option>
                                <option value="Македония">Македония</option>
                                <option value="Малави">Малави</option>
                                <option value="Малайзия">Малайзия</option>
                                <option value="Мали">Мали</option>
                                <option value="Мальдивы">Мальдивы</option>
                                <option value="Мальта">Мальта</option>
                                <option value="Марокко">Марокко</option>
                                <option value="Мартиника">Мартиника</option>
                                <option value="Маршалловы Острова">Маршалловы Острова</option>
                                <option value="Мексика">Мексика</option>
                                <option value="Микронезия">Микронезия</option>
                                <option value="Мозамбик">Мозамбик</option>
                                <option value="Молдавия">Молдавия</option>
                                <option value="Монако">Монако</option>
                                <option value="Монголия">Монголия</option>
                                <option value="Монсеррат">Монсеррат</option>
                                <option value="Монтенегро">Монтенегро</option>
                                <option value="Мьянма (Бирма)">Мьянма (Бирма)</option>
                                <option value="Норвегия">Норвегия</option>
                                <option value="Новая Каледония">Новая Каледония</option>
                                <option value="Новая Зеландия">Новая Зеландия</option>
                                <option value="Ниу острова">Ниу острова</option>
                                <option value="Никарагуа">Никарагуа</option>
                                <option value="Нидерланды">Нидерланды</option>
                                <option value="Нидерландские Антильские острова">Нидерландские Антильские острова</option>
                                <option value="Нигерия">Нигерия</option>
                                <option value="Нигер">Нигер</option>
                                <option value="Непал">Непал</option>
                                <option value="Невис">Невис</option>
                                <option value="Острова Кука">Острова Кука</option>
                                <option value="Объединённые Арабские Эмираты">Объединённые Арабские Эмираты</option>
                                <option value="Остров Святой Елены">Остров Святой Елены</option>
                                <option value="Оман">Оман</option>
                                <option value="Пакистан">Пакистан</option>
                                <option value="Палау">Палау</option>
                                <option value="Панама">Панама</option>
                                <option value="Папуа Новая Гвинея">Папуа Новая Гвинея</option>
                                <option value="Парагвай">Парагвай</option>
                                <option value="Перу">Перу</option>
                                <option value="Польша">Польша</option>
                                <option value="Португалия">Португалия</option>
                                <option value="Пуэрто-Рико">Пуэрто-Рико</option>
                                <option value="Республика Гаити">Республика Гаити</option>
                                <option value="Румыния">Румыния</option>
                                <option value="Руанда">Руанда</option>
                                <option value="Реюньон">Реюньон</option>
                                <option value="Сайпан">Сайпан</option>
                                <option value="Сальвадор">Сальвадор</option>
                                <option value="Самоа">Самоа</option>
                                <option value="Сан-Марино">Сан-Марино</option>
                                <option value="Санта-Лючия">Санта-Лючия</option>
                                <option value="Сан-Томе и Принсипи">Сан-Томе и Принсипи</option>
                                <option value="Саудовская Аравия">Саудовская Аравия</option>
                                <option value="Свазиленд">Свазиленд</option>
                                <option value="Северная Корея">Северная Корея</option>
                                <option value="Сейшельские Острова">Сейшельские Острова</option>
                                <option value="Сен-Бартелеми">Сен-Бартелеми</option>
                                <option value="Сенегал">Сенегал</option>
                                <option value="Сен-Мартен">Сен-Мартен</option>
                                <option value="Сент-Винсент и Гренадины">Сент-Винсент и Гренадины</option>
                                <option value="Сент-Китс">Сент-Китс</option>
                                <option value="Сербия">Сербия</option>
                                <option value="Сингапур">Сингапур</option>
                                <option value="Синт-Эстатиус">Синт-Эстатиус</option>
                                <option value="Сирия">Сирия</option>
                                <option value="Словакия">Словакия</option>
                                <option value="Словения">Словения</option>
                                <option value="Соломоновы Острова">Соломоновы Острова</option>
                                <option value="Сомали">Сомали</option>
                                <option value="Сомалиленд">Сомалиленд</option>
                                <option value="Судан">Судан</option>
                                <option value="Суринам">Суринам</option>
                                <option value="США">США</option>
                                <option value="Сьерра-Леоне">Сьерра-Леоне</option>
                                <option value="Таджикистан">Таджикистан</option>
                                <option value="Тайвань">Тайвань</option>
                                <option value="Таиланд">Таиланд</option>
                                <option value="Таити">Таити</option>
                                <option value="Танзания">Танзания</option>
                                <option value="Теркс и Кайкос">Теркс и Кайкос</option>
                                <option value="Того">Того</option>
                                <option value="Тонга">Тонга</option>
                                <option value="Тринидад и Тобаго">Тринидад и Тобаго</option>
                                <option value="Уругвай">Уругвай</option>
                                <option value="Украина">Украина</option>
                                <option value="Узбекистан">Узбекистан</option>
                                <option value="Уганда">Уганда</option>
                                <option value="Фарерские острова">Фарерские острова</option>
                                <option value="Фиджи">Фиджи</option>
                                <option value="Филиппины">Филиппины</option>
                                <option value="Финляндия">Финляндия</option>
                                <option value="Фолклендские острова">Фолклендские острова</option>
                                <option value="Франция">Франция</option>
                                <option value="Французская Гвиана">Французская Гвиана</option>
                                <option value="Хорватия">Хорватия</option>
                                <option value="Центральноафриканская Республика">Центральноафриканская Республика</option>
                                <option value="Чад">Чад</option>
                                <option value="Чехия">Чехия</option>
                                <option value="Чили">Чили</option>
                                <option value="Швейцария">Швейцария</option>
                                <option value="Шри-Ланка">Шри-Ланка</option>
                                <option value="Швеция">Швеция</option>
                                <option value="Эквадор">Эквадор</option>
                                <option value="Экваториальная Гвинея">Экваториальная Гвинея</option>
                                <option value="Эритрея">Эритрея</option>
                                <option value="Эстония">Эстония</option>
                                <option value="Эфиопия">Эфиопия</option>
                                <option value="Южная Корея">Южная Корея</option>
                                <option value="Южно-Африканская Республика">Южно-Африканская Республика</option>
                                <option value="Ямайка">Ямайка</option>
                                <option value="Япония">Япония</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="weight-input">вес груза (кг)</label>
                            <input name="weight" type="text" class="form-control" id="weight-input" required>
                        </div>
                        <div class="form-group">
                            <label for="delivery-method-input">Способ доставки</label>
                            <select name="delivery-method" id="delivery-method-input" required>
                                <option selected value=""></option>
                                <option value="Экспресс">Экспресс</option>
                                <option value="Авиа">Авиа</option>
                                <option value="Море">Море</option>
                                <option value="Ж/Д">Ж/Д</option>
                            </select>
                        </div>

                        <div class="form-group textarea-group">
                            <label for="question-input2">ваш вопрос</label>
                            <textarea name="question" id="question-input2" required></textarea>
                        </div>

                        <p>После отправки нам заполненной формы, наш специалист свяжется с вами в течение рабочего дня и
                            сориентирует вас по цене, по срокам доставки, а также ответит на все интересующие вопросы.
                        </p>

                        <button type="submit" class="button">рассчитать</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="content col-12">
                    <p>Wingo 2009-2018</p>
                    <p>Copyright ©</p>
                </div>
            </div>
        </div>
    </footer>
</div>

<?php include ('components/popup.php');?>


<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 7]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
<![endif]-->
<!--[if lt IE 8]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="js/bootstrap.min.js"></script>

<script src="js/wow.min.js"></script>
<script src="js/likely.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/jquery.viewportchecker.min.js"></script>
<script src="js/js.js"></script>
</body>
</html>
